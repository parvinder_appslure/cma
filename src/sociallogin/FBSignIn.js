import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import auth from '@react-native-firebase/auth';
import {LoginManager, AccessToken} from 'react-native-fbsdk';

const FBSignIn = () => {
  const onPressHandler = async () => {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    consolejson(result);
    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();
    consolejson(data);
    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(
      data.accessToken,
    );
    consolejson(facebookCredential);
    // Sign-in the user with the credential
    const res = await auth().signInWithCredential(facebookCredential);
    consolejson(res);
  };
  return (
    <TouchableOpacity onPress={onPressHandler} style={styles.touch}>
      <Text style={styles.textTitle}>{`CONTINUE WITH FACEBOOK`}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touch: {
    backgroundColor: '#3B5998',
    padding: 15,
    marginHorizontal: 25,
    borderRadius: 10,
    marginTop: 20,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
});

export default FBSignIn;
