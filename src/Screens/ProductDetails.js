import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const Productdetails = ({navigation}) => {
  return (
    <MainView>
      <View
        style={{
          height: 54,
          width: Dimensions.get('window').width,
          marginTop: 20,
          flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <View
          style={{
            width: '83%',
            flexDirection: 'row',
            marginLeft: 20,
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={{height: 22, width: 12, resizeMode: 'contain'}}
              source={require('../assets/back.png')}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity>
          <View style={{alignSelf: 'flex-end'}}>
            <Image
              style={{height: 24, width: 25, resizeMode: 'contain'}}
              source={require('../assets/cart.png')}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          height: 0.6,
          width: '100%',
          alignSelf: 'center',
          backgroundColor: '#FFFFFF40',
        }}></View>
      <ScrollView>
        <View style={{width: '90%', alignSelf: 'center', marginTop: 20}}>
          <Text style={styles.middleText}>Product Details</Text>

          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              marginTop: 5,
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>Height: 5.3 inches</Text>
          </View>

          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              marginTop: 5,
              alignItems: 'center',
              // opacity: 0.6,
            }}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>Diameter: 4.2 inches</Text>
          </View>

          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              marginTop: 5,
              alignItems: 'center',
              // opacity: 0.6,
            }}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>Width: 1.7 inches</Text>
          </View>

          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              marginTop: 5,
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>Weight: 500 gm</Text>
          </View>

          <Text style={styles.middleText}>Description</Text>

          <Text style={styles.tttText}>
            Jyotirling Wall Hanging Yantra displays the auspicious image of Lord
            Shiva. Lord Shiva manifested himself as a Jyotirlingam in the night
            of Aridra Nakshatras; it was on the 14th day in the dark half of the
            month of Phalguna that Lord Shiva first manifested himself in the
            form of a Linga. The Jyotirlinga are ancient shrines of immense
            importance and significance where Lord Shiva is worshipped as a
            Lingam or fiery column of light.
          </Text>

          <Text style={styles.middleText}>Delivery and Returns</Text>

          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              marginTop: 5,
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>Free delivery on all orders</Text>
          </View>

          <View style={{width: '90%', flexDirection: 'row', marginTop: 5}}>
            <View
              style={{
                width: 5,
                height: 5,
                backgroundColor: '#FFF',
                borderRadius: 2.5,
                marginTop: 15,
                opacity: 0.6,
              }}></View>
            <Text style={styles.topTexting}>
              30-days no question return / exchange policy
            </Text>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate('ReviewYourOrder')}
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 40,
            marginBottom: 40,
            justifyContent: 'center',
            backgroundColor: '#3F55F6',
            borderRadius: 10,
          }}>
          <Text
            style={{
              alignSelf: 'center',
              marginVertical: 15,
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Medium',
              color: '#FFF',
              fontWeight: '500',
            }}>
            Add to Cart
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },

  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
    marginLeft: 10,
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topTexting: {
    lineHeight: 35,
    marginLeft: 10,
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.6,
  },
  tttText: {
    marginTop: 10,
    textAlign: 'justify',
    lineHeight: 23,
    fontFamily: 'Avenir-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#FFFFFF',
    opacity: 0.6,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 20,
  },
});

export default Productdetails;
