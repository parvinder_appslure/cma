import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Button, ButtonStyle, MainView} from '../Custom/CustomView';
import FBSignIn from '../sociallogin/FBSignIn';
import GoogleSignIn from '../sociallogin/GoogleSignIn';
const {height} = Dimensions.get('window');

const SelectOption = ({navigation}) => {
  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Image source={require('../assets/logo.png')} style={styles.logo} />

        <View>
          <FBSignIn />
          <GoogleSignIn />
          {/* {ButtonStyle('CONTINUE WITH FACEBOOK', '#3B5998', '#FFFFFF')} */}
          {/* {ButtonStyle('CONTINUE WITH GOOGLE', '#ffffff', '#000000')} */}
          {/* {ButtonStyle('CONTINUE WITH INSTAGRAM', '#D73085', '#FFFFFF')} */}
          {/* {ButtonStyle('CONTINUE WITH APPLE ID', '#FFFFFF', '#000000')} */}

          {ButtonStyle('CREATE AN ACCOUNT', '#3F55F6', '#FFFFFF', () =>
            navigation.navigate('SignUp'),
          )}
        </View>

        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.bottomtext}>
            Have Call My Astro account?
            <Text style={[styles.bottomtext, {color: '#6266F9'}]}> Log in</Text>
          </Text>
        </TouchableOpacity>
        <View style={styles.bottomview}>
          <Text style={styles.bottomtext}>
            {`By Signing up, I argee to our `}
            <Text style={[styles.bottomtext, {color: '#6266F9'}]}>
              {`Terms of Service `}
            </Text>
            {`and that you have read our `}
            <Text style={[styles.bottomtext, {color: '#6266F9'}]}>
              {`Privacy Policy`}
            </Text>
          </Text>
        </View>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  logo: {
    marginTop: '10%',
    marginBottom: '20%',
    width: 196,
    height: 196,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  facebookButton: {
    backgroundColor: '#3B5998',
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 10,
    marginTop: 50,
  },
  facebooktext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  bottomtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 20,
    lineHeight: 20,
  },
  bottomview: {
    marginHorizontal: 25,
    textAlign: 'center',
  },
});

export default SelectOption;
