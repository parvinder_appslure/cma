import React, {useState, memo, useMemo, useContext, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import {useDispatch, useStore} from 'react-redux';
import * as actions from '../../redux/actions';
import {useNavigation} from '@react-navigation/native';
import {width} from '../../services/Api';
import {SocketContext} from '../../redux/context';
import Global from '../Global';
const {Agora} = NativeModules;
const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  Audience,
  Adaptative,
} = Agora;

const setCallType = (title, type) => {
  switch (type) {
    case 1:
      return (title += 'Video');
    case 2:
      return (title += 'Audio');
    case 3:
      return (title += 'Chat');
    default:
      return '';
  }
};
const RequestHandler = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const socket = useContext(SocketContext);
  const {userDetail} = useStore().getState();
  const [state, setState] = useState({
    status: false,
    user_id: userDetail.id,
    data: {},
  });
  useEffect(() => {
    socket.on('get_booking_status', response => {
      console.log('\nSocket Response :: get_booking_status\n');
      console.log(Object.keys(response));
      const {status = false, data = {}} = response;
      console.log('status_txt', data?.status_txt);
      console.log('is_accepted', data?.is_accepted);
      setState({...state, status, data});
    });
    socket.emit('get_booking_status', {user_id: state.user_id});

    socket.on('give_review', response => {
      if (response.status) {
        // navigation.reset({
        //   index: 1,
        //   routes: [{name: 'DrawerNavigator'}, {name: 'Rating'}],
        // });
      }
    });
    socket.emit('give_review', {user_id: state.user_id});

    socket.on('user_update', ({status = false, data = {}, message = ''}) => {
      if (status && Object.keys(data).length !== 0) {
        dispatch(actions.SetUserDetail(data));
      }
    });
    socket.emit('user_update', {user_id: state.user_id});
  }, []);

  const onSocketResponseHandler = what => {
    const {
      user_id,
      data: {id = ''},
    } = state;
    if (id) {
      socket.emit('accept_reject_user_booking', {
        user_id,
        what,
        booking_id: id,
      });
    } else alert('Booking id not found');
  };

  const onCancelHandler = () => {
    console.log('\nCancel Handler:: Cancel');
    socket.emit('reject_request', {
      user_id: state.user_id,
    });
  };
  const onRejectHandler = () => {
    console.log('\nReject Handler:: Reject');
    onSocketResponseHandler('2');
  };

  const onJoinHandler = () => {
    const {
      user_id,
      data: {
        id = '',
        bridge_id = '',
        is_accepted = '',
        remaining_time = '',
        assign_id = '',
        type = '',
        astrologer = {},
      },
    } = state;
    if ([0, 1].includes(is_accepted)) {
      console.log(`\nJoin Handler:: Status :: ${is_accepted}`);
      if (id) {
        is_accepted === 0 && onSocketResponseHandler('1');
        Global.rtime = remaining_time;
        Global.bookingid = bridge_id;
        Global.ids = id;
        Global.another = assign_id;
        Global.user_id = user_id;
        Global.data = state.data;
        Global.pimage = astrologer?.imageUrl || 'url';
        Global.priest_name = astrologer?.name || 'Guest';
        if (type === 1) {
          console.log('video');
          navigation.navigate('LiveVideoCall', {
            uid: Math.floor(Math.random() * 100),
            clientRole: Host,
            channelName: bridge_id,
            onCancel: message => {},
          });
        }
        if (type === 2) {
          console.log('audio');
          navigation.navigate('LiveAudioCall', {
            uid: Math.floor(Math.random() * 100),
            clientRole: Host,
            channelName: bridge_id,
            onCancel: message => {},
          });
        }
        if (type === 3) {
          navigation.navigate('LiveChat');
        }
      } else {
        alert('booking id not found');
      }
    }
  };

  const title = useMemo(() => {
    const {
      data: {type = '', status_txt = '', astrologer = {}},
    } = state;
    if (status_txt === 'Pending') {
      return setCallType('Request for ', type);
    }
    if (status_txt === 'Accepted') {
      return setCallType(
        `Astrologer ${astrologer?.name} waiting for you to join on `,
        type,
      );
    }
    return '';
  }, [state.data]);

  return state.status ? (
    <View style={styles.container}>
      {state.data?.status_txt === 'Pending' && (
        <View style={styles.containerPending}>
          <Text style={styles.textTitle}>{title}</Text>
          <TouchableOpacity
            style={styles.touchCancel}
            onPress={onCancelHandler}>
            <Text style={styles.textButton}>Cancel</Text>
          </TouchableOpacity>
        </View>
      )}
      {state.data?.status_txt === 'Accepted' && (
        <View
          style={
            state.data?.is_accepted === 0
              ? styles.containerAccept
              : styles.containerPending
          }>
          <Text style={styles.textTitle}>{title}</Text>
          <View style={state.data?.is_accepted === 0 && styles.viewButton}>
            <TouchableOpacity style={styles.touchJoin} onPress={onJoinHandler}>
              <Text style={styles.textButton}>Join</Text>
            </TouchableOpacity>
            {state.data?.is_accepted === 0 && (
              <TouchableOpacity
                style={styles.touchCancel}
                onPress={onRejectHandler}>
                <Text style={styles.textButton}>Reject</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      )}
    </View>
  ) : null;
};

export default memo(RequestHandler);
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: 'white',
    alignSelf: 'center',
    bottom: 10,
    width: width - 40,
    borderRadius: 10,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerPending: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerAccept: {},

  viewButton: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-evenly',
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#4FA980',
    flex: 1,
  },
  textButton: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
    alignSelf: 'center',
  },
  touchCancel: {
    backgroundColor: 'red',
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 5,
    width: 80,
  },
  touchJoin: {
    backgroundColor: '#4FA980',
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 5,
    width: 80,
  },
});

// GLOBAL.pimage = pujadata.astrologer.imageUrl
// GLOBAL.priest_name = pujadata.astrologer.name
// navigation.navigate("LiveVideoCall", {
//                           uid: Math.floor(Math.random() * 100),
//                           clientRole: Host,
//                           channelName: pujadata.bridge_id,
//                           onCancel: (message) => {
//                             setvisible(true)
