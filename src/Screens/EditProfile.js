import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {
  BottomButton,
  CustomInputField,
  Header,
  MainView,
} from '../Custom/CustomView';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {useSelector} from 'react-redux';
import {Api} from '../services/Api';

const EditProfile = ({navigation}) => {
  const {userDetail} = useSelector(store => store);
  const [state, setState] = useState({
    name: userDetail.name,
    user_id: userDetail.id,
    email: userDetail.email,
    phone: userDetail.phone,
    gender: userDetail.gender,
    address: userDetail.address,
    dob: userDetail.dob,
    birth_time: userDetail.birth_time,
    place_of_birth: userDetail.place_of_birth,
    modalVisible: false,
    isLoading: false,
    showPicker: false,
    date: '',
    mode: '',
    uri: '',
    type: '',
    fileName: '',
  });

  const toggleModal = modalVisible => setState({...state, modalVisible});
  const toggleLoader = isLoading => setState({...state, isLoading});
  const onImageOptionHandler = launch => {
    toggleModal(false);
    const options = {
      title: 'Select and Take Profile Picture',
      cameraType: 'front',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launch(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const {type, fileName, fileSize, uri} = response.assets[0];
        console.log(JSON.stringify(response.assets[0], null, 2));
        setState({...state, uri, type, fileName, modalVisible: false});
      }
    });
  };

  const onSubmitHandler = async () => {
    const {name, gender, phone, uri, type, fileName, user_id} = state;
    consolejson(state);
    if (
      uri === '' &&
      name === userDetail.name &&
      phone === userDetail.phone &&
      gender === userDetail.gender
    ) {
      return;
    }

    if (name === '') {
      alert('Enter your name');
      return;
    }
    if (phone.length !== 10 && !isNaN(phone)) {
      alert('Please enter valid Mobile number');
      return;
    }
    if (gender === '') {
      alert('Select your gender');
      return;
    }
    toggleLoader(true);
    const flag = uri === '' ? 0 : 1;
    alert(user_id);
    let formdata = new FormData();
    formdata.append('user_id', user_id);
    formdata.append('name', name);
    formdata.append('phone', phone);
    formdata.append('gender', gender);
    formdata.append('flag', flag);
    flag == 1 && formdata.append('image', {uri, type, name: fileName});
    const response = await Api.updateProfile(formdata);
    toggleLoader(false);
    consolejson(response);
  };

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header onPress={navigation.goBack} title={'Profile'} />
        <ImageBackground
          style={styles.userimage}
          imageStyle={styles.imageStyle}
          resizeMode="cover"
          source={{uri: state.uri || userDetail.imageUrl}}>
          <TouchableOpacity onPress={() => toggleModal(true)}>
            <Image
              style={styles.editImage}
              source={require('../assets/edit-1.png')}
            />
          </TouchableOpacity>
        </ImageBackground>

        <CustomInputField
          label="Name"
          defaultValue={state.name}
          onChangeText={name => setState({...state, name})}
        />
        <CustomInputField
          label="Email"
          defaultValue={state.email}
          editable={false}
        />

        <CustomInputField
          keyboardType="number-pad"
          label="Mobile Number"
          defaultValue={state.phone}
          editable={false}
        />

        <Text style={styles.middletext}>Gender</Text>
        <View style={styles.viewGender}>
          <TouchableOpacity
            style={{marginRight: 30}}
            onPress={() => setState({...state, gender: 'male'})}>
            <Image
              source={require('../assets/male.png')}
              style={styles.imageGender}
            />
            {state.gender === 'male' && (
              <Image
                source={require('../assets/checked.png')}
                style={styles.imageTick}
              />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setState({...state, gender: 'female'})}>
            <Image
              source={require('../assets/female.png')}
              style={styles.imageGender}
            />
            {state.gender === 'female' && (
              <Image
                source={require('../assets/checked.png')}
                style={styles.imageTick}
              />
            )}
          </TouchableOpacity>
        </View>

        <BottomButton onPress={onSubmitHandler} bottomtitle={'SAVE'} />
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={state.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <TouchableOpacity
          style={modalStyle.container}
          onPress={() => toggleModal(false)}>
          <View style={modalStyle.modalView}>
            <TouchableOpacity
              style={modalStyle.touch}
              onPress={() => onImageOptionHandler(launchCamera)}>
              <Text style={modalStyle.text}>Click a Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={modalStyle.touch}
              onPress={() => onImageOptionHandler(launchImageLibrary)}>
              <Text style={modalStyle.text}>Choose Photo from Gallery</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    </MainView>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  userimage: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    marginTop: 20,
    alignSelf: 'center',
  },
  editimage: {
    width: 28,
    height: 28,
    resizeMode: 'contain',
    // marginTop: -10,
    alignSelf: 'flex-end',
  },
  middletext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '400',
    color: '#FFFFFF',
    marginTop: 25,
    marginHorizontal: 25,
  },
  genderstyle: {
    flexDirection: 'row',
    marginHorizontal: 25,
    marginTop: 20,
  },
  genderImage: {
    width: 55,
    height: 55,
    resizeMode: 'contain',
  },
  genderText: {
    fontSize: 14,
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    color: '#FFFFFF',
    marginTop: 10,
  },

  imageStyle: {
    borderColor: '#ffffff',
    borderWidth: 0.5,
    borderRadius: 75,
  },
  editImage: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },

  viewGender: {
    flexDirection: 'row',
    margin: 30,
    marginBottom: 10,
  },
  imageGender: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
  },
  imageTick: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    position: 'absolute',
    right: 2,
    top: 2,
  },
});

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  modalView: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginHorizontal: 30,
    padding: 20,
  },
  touch: {
    margin: 5,
    padding: 5,
  },
  text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#1E2432',
  },
});
