import React from 'react';
import {
  Image,
  Linking,
  Share,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {packageName} from '../services/Config';

const Setting = ({navigation}) => {
  const onLogoutHandler = () => {
    console.log('logout');
  };
  const onShare = async () => {
    try {
      const result = await Share.share({
        title: 'App link',
        message: `Please install this app and stay safe , AppLink :https://play.google.com/store/apps/details?id=${packageName}&hl=en`,
        url: `https://play.google.com/store/apps/details?id=${packageName}&hl=en`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={'Settings'} />
      <View style={styles.middleView}>
        <TouchableOpacity style={styles.topbox} onPress={onShare}>
          <Image
            style={styles.shareimage}
            source={require('../assets/share.png')}
          />
          <Text style={styles.topText}>Share App</Text>
        </TouchableOpacity>

        <View style={styles.line} />

        <TouchableOpacity
          style={styles.topbox}
          onPress={() => {
            Linking.openURL(
              `http://play.google.com/store/apps/details?id=${packageName}`,
            );
          }}>
          <Image
            style={styles.shareimage}
            source={require('../assets/like_menu.png')}
          />
          <Text style={styles.topText}>Rate Us</Text>
        </TouchableOpacity>

        <View style={styles.line} />

        <TouchableOpacity
          style={styles.topbox}
          onPress={() => {
            navigation.navigate('Information', {
              title: 'Privacy Policy',
              key: 'privacy',
            });
          }}>
          <Image
            style={styles.shareimage}
            source={require('../assets/password.png')}
          />
          <Text style={styles.topText}>Privacy Policy</Text>
        </TouchableOpacity>

        <View style={styles.line} />

        <TouchableOpacity
          style={styles.topbox}
          onPress={() => {
            navigation.navigate('Information', {
              title: 'Term & Condition',
              key: 'terms_condition',
            });
          }}>
          <Image
            style={styles.shareimage}
            source={require('../assets/document_menu.png')}
          />
          <Text style={styles.topText}>Terms & Condition</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.middleView}>
        <TouchableOpacity style={styles.topbox} onPress={onLogoutHandler}>
          <Image
            style={styles.shareimage}
            source={require('../assets/logout_menu.png')}
          />
          <Text style={styles.topText}>Logout</Text>
        </TouchableOpacity>
      </View>
    </MainView>
  );
};

export default Setting;

const styles = StyleSheet.create({
  middleView: {
    backgroundColor: '#13042A',
    margin: 20,
    borderRadius: 15,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: '#FFFFFF33',
  },
  topbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  shareimage: {
    width: 18,
    height: 20,
    resizeMode: 'contain',
  },
  topText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '400',
    color: '#FFFFFF',
    marginHorizontal: 20,
  },
  line: {
    backgroundColor: '#ffffff1a',
    height: 1,
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10,
  },
});
