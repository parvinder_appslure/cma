import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, FlatList, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView, TextError} from '../Custom/CustomView';
import * as actions from '../redux/actions';
import {Api} from '../services/Api';
const UserList = ({navigation}) => {
  const [people, setPeople] = useState([
    {
      key: 1,
      name: 'Rahul Sharma',
      gender: 'Male  |  25 yrs',
      source: require('../assets/dp.png'),
      image: require('../assets/delete-2.png'),
      image1: require('../assets/edit-2.png'),
      relation: 'Relation : Brother',
      date: '19-12-20, 01:30 PM',
      place: 'New Delhi, Delhi',
    },

    {
      key: 2,
      name: 'Rahul Sharma',
      gender: 'Male  |  25 yrs',
      source: require('../assets/dp.png'),
      image: require('../assets/delete-2.png'),
      image1: require('../assets/edit-2.png'),
      relation: 'Relation : Brother',
      date: '19-12-20, 01:30 PM',
      place: 'New Delhi, Delhi',
    },

    {
      key: 3,
      name: 'Rahul Sharma',
      gender: 'Male  |  25 yrs',
      source: require('../assets/dp.png'),
      image: require('../assets/delete-2.png'),
      image1: require('../assets/edit-2.png'),
      relation: 'Relation : Brother',
      date: '19-12-20, 01:30 PM',
      place: 'New Delhi, Delhi',
    },

    {
      key: 4,
      name: 'Rahul Sharma',
      gender: 'Male  |  25 yrs',
      source: require('../assets/dp.png'),
      image: require('../assets/delete-2.png'),
      image1: require('../assets/edit-2.png'),
      relation: 'Relation : Brother',
      date: '19-12-20, 01:30 PM',
      place: 'New Delhi, Delhi',
    },
  ]);

  const dispatch = useDispatch();
  const {membersList} = useSelector(store => store);
  useEffect(() => {
    (async () => {
      if (membersList.length === 0) {
        const response = await Api.membersList();
        const {status = false, data = []} = response;
        // consolejson(response);
        status
          ? dispatch(actions.SetMembersList(data))
          : alert('Something went wrong');
      }
    })();
  }, []);
  const onDeleteHandler = () => {};
  return (
    <MainView>
      <StatusBarLight />
      <Header title={'User List'} onPress={navigation.goBack} />

      {membersList.length !== 0 ? (
        <View style={{paddingVertical: 10}}>
          <FlatList
            data={membersList}
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => {
              return (
                <View style={styles.flv_Style}>
                  <View style={styles.flv1_Style}>
                    <Image
                      source={require('../assets/dp.png')}
                      style={styles.fli_Style}
                    />
                    <View style={styles.flv2_Style}>
                      <Text style={styles.flt_Style}>{item?.name}</Text>
                      <Text style={styles.flt1_Style}>{item?.gender}</Text>
                      <Text style={styles.flt1_Style}>{item?.relation}</Text>
                      <Text style={styles.flt1_Style}>{item?.date}</Text>
                      <Text style={styles.flt1_Style}>{item?.place}</Text>
                    </View>
                    <Image
                      source={require('../assets/edit-2.png')}
                      style={styles.fli1_Style}
                    />
                    <Image
                      source={require('../assets/delete-2.png')}
                      style={styles.fli1_Style}
                    />
                  </View>
                </View>
              );
            }}
          />
        </View>
      ) : (
        <TextError title={'Members not found'} />
      )}
    </MainView>
  );
};

export default UserList;

const styles = StyleSheet.create({
  flv_Style: {
    backgroundColor: '#13042A',
    padding: 10,
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 10,
    // borderColor: '#69707F',
    borderWidth: 1,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },

  fli_Style: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  flv1_Style: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 20,
  },

  flv2_Style: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },

  flt_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },

  flt1_Style: {
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 6,
  },

  fli1_Style: {
    width: 15,
    height: 18,
    resizeMode: 'contain',
  },
});
