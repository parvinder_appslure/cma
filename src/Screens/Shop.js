import React from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';
import {useState} from 'react';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const Shop = ({navigation, route}) => {
  const [listdata, setlistData] = useState([
    {
      keys: 1,
      profilesource: require('../assets/Group1.png'),
      title: 'Books',
      nav: 'Decoration',
    },
    {
      keys: 2,
      profilesource: require('../assets/Group2.png'),
      title: 'Rings',
      nav: 'Decoration',
    },
    {
      keys: 3,
      profilesource: require('../assets/Group3.png'),
      title: 'Yantra',
      nav: 'Decoration',
    },
    {
      keys: 4,
      profilesource: require('../assets/Group4.png'),
      title: 'Gutika',
      nav: 'Decoration',
    },
    {
      keys: 5,
      profilesource: require('../assets/Group1.png'),
      title: 'Rosary',
      nav: 'Decoration',
    },
    {
      keys: 6,
      profilesource: require('../assets/Group2.png'),
      title: 'Rings',
      nav: 'Decoration',
    },
  ]);

  const [shopList, setshopList] = useState([
    {
      keys: 1,
      profilesource: require('../assets/c1.png'),
      title: 'Gemstones',
      nav: 'Decoration',
    },
    {
      keys: 2,
      profilesource: require('../assets/c2.png'),
      title: 'Gutika',
      nav: 'Decoration',
    },
    {
      keys: 3,
      profilesource: require('../assets/c3.png'),
      title: 'Decoration',
      nav: 'Decoration',
    },
    {
      keys: 4,
      profilesource: require('../assets/c1.png'),
      title: 'Rosary',
      nav: 'Decoration',
    },
    {
      keys: 5,
      profilesource: require('../assets/c2.png'),
      title: 'Rudraksh',
      nav: 'Decoration',
    },
    {
      keys: 6,
      profilesource: require('../assets/c3.png'),
      title: 'Yantra',
      nav: 'Decoration',
    },
    {
      keys: 7,
      profilesource: require('../assets/c1.png'),
      title: 'Puja Items',
      nav: 'Decoration',
    },
    {
      keys: 8,
      profilesource: require('../assets/c2.png'),
      title: 'Rings',
      nav: 'Decoration',
    },
    {
      keys: 9,
      profilesource: require('../assets/c3.png'),
      title: 'Bracelets',
      nav: 'Decoration',
    },
  ]);

  const [bestSllersList, setBestSllersList] = useState([
    {
      keys: 1,
      source: require('../assets/3.png'),
      nav: 'AddToCart',
    },
    {
      keys: 2,
      source: require('../assets/3.png'),
      nav: 'AddToCart',
    },
    {
      keys: 3,
      source: require('../assets/3.png'),
      nav: 'AddToCart',
    },
    {
      keys: 4,
      source: require('../assets/3.png'),
      nav: 'AddToCart',
    },
    {
      keys: 5,
      source: require('../assets/3.png'),
      nav: 'AddToCart',
    },
  ]);

  return (
    <MainView>
      <SafeAreaProvider>
        <StatusBar />
        <ScrollView>
          <View
            style={{
              height: 54,
              width: Dimensions.get('window').width,
              marginTop: 20,
            }}>
            <View
              style={{
                width: '90%',
                alignSelf: 'center',
                flexDirection: 'row',
                marginVertical: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={navigation.goBack}>
                <Image
                  style={{height: 22, width: 12, resizeMode: 'contain'}}
                  source={require('../assets/back.png')}
                />
              </TouchableOpacity>

              <Text
                style={{
                  color: '#FFF',
                  fontSize: 20,
                  lineHeight: 30,
                  fontFamily: 'Avenir-Heavy',
                  fontWeight: '700',
                }}>
                Shop
              </Text>

              <TouchableOpacity>
                <Image
                  style={{height: 18, width: 25, resizeMode: 'contain'}}
                  source={require('../assets/cart.png')}
                />
              </TouchableOpacity>
            </View>
          </View>

          <FlatList
            style={{width: '100%'}}
            data={listdata}
            horizontal={true}
            // showsHorizontalScrollIndicator={false}
            // renderItem={renderItemListData}
            renderItem={({item}) => {
              return (
                <View style={{marginHorizontal: 10}}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate(item.nav)}
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 35,
                      justifyContent: 'center',
                      backgroundColor: '#7A7A7A40',
                      borderWidth: 1,
                      borderColor: '#3F55F6',
                    }}>
                    <Image style={styles.catImg} source={item.profilesource} />
                  </TouchableOpacity>
                  <Text style={styles.catTextss}>{item.title}</Text>
                </View>
              );
            }}
          />
          <View style={{width: '90%', alignSelf: 'center', marginTop: 20}}>
            <ImageBackground
              imageStyle={{borderRadius: 12}}
              style={{alignSelf: 'center', width: '100%'}}
              source={require('../assets/girl1.png')}>
              <Text
                style={{
                  color: '#FFF',
                  fontSize: 30,
                  lineHeight: 35,
                  marginTop: 275,
                  alignSelf: 'center',
                  fontFamily: 'Avenir-Heavy',
                  fontWeight: '900',
                }}>
                Pukhraj
              </Text>

              <Text
                style={{
                  color: '#FFF',
                  fontSize: 15,
                  lineHeight: 19,
                  marginTop: 2,
                  alignSelf: 'center',
                  fontFamily: 'Avenir-Medium',
                  fontWeight: '500',
                }}>
                Flat 30% OFF on all Pukhraj
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  marginTop: 17,
                  alignItems: 'center',
                  marginBottom: 35,
                }}>
                <Text
                  style={{
                    color: '#FFF',
                    fontSize: 15,
                    lineHeight: 19,
                    fontFamily: 'Avenir-Medium',
                    fontWeight: '500',
                  }}>
                  BUY NOW
                </Text>

                <Image
                  style={{
                    height: 10,
                    width: 6,
                    resizeMode: 'contain',
                    marginLeft: 10,
                  }}
                  source={require('../assets/right-arrow.png')}
                />
              </View>
            </ImageBackground>
          </View>
          <Text
            style={{
              color: '#FFF',
              fontSize: 18,
              lineHeight: 24,
              marginLeft: 20,
              fontFamily: 'Avenir-Heavy',
              fontWeight: '900',
              marginVertical: 10,
            }}>
            Shop by Category
          </Text>

          <FlatList
            style={{width: '100%'}}
            data={shopList}
            numColumns={3}
            horizontal={false}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => {
              return (
                <View style={{marginHorizontal: 10}}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate(item.nav)}
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 50,
                      justifyContent: 'center',
                      backgroundColor: '#7A7A7A40',
                      borderWidth: 1,
                      borderColor: '#3F55F6',
                    }}>
                    <Image style={styles.catImgs} source={item.profilesource} />
                  </TouchableOpacity>
                  <Text style={styles.catTexts}>{item.title}</Text>
                </View>
              );
            }}
          />

          <ImageBackground
            imageStyle={{borderRadius: 12}}
            style={{alignSelf: 'center', width: '100%', marginBottom: 5}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
              }}>
              <View
                style={{flexDirection: 'column', width: 110, marginLeft: 15}}>
                <Text
                  style={{
                    color: '#FFF',
                    fontSize: 26,
                    lineHeight: 30,
                    width: 90,
                    fontFamily: 'Avenir-Heavy',
                    fontWeight: '900',
                    marginVertical: 10,
                    marginTop: -50,
                  }}>
                  Best Sellers
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    width: 110,
                    alignItems: 'center',
                    marginTop: 50,
                  }}>
                  <Text
                    style={{
                      color: '#FFF',
                      fontSize: 15,
                      lineHeight: 19,
                      fontFamily: 'Avenir-Heavy',
                      fontWeight: '900',
                      // marginTop: -20,
                    }}>
                    SEE ALL
                  </Text>

                  <Image
                    style={{
                      height: 12,
                      width: 12,
                      resizeMode: 'contain',
                      marginLeft: 10,
                    }}
                    source={require('../assets/right-arrow.png')}
                  />
                </View>
              </View>

              <FlatList
                style={{width: '100%'}}
                data={bestSllersList}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                // renderItem={renderBestSellers}

                renderItem={({item}) => {
                  return (
                    <View
                      //onPress={()=>navigation.navigate('')}

                      style={{marginHorizontal: 10}}>
                      <View
                        style={{
                          width: '100%',
                          borderRadius: 10,
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={() => navigation.navigate(item.nav)}>
                          <Image
                            style={styles.catseller}
                            source={item.source}
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={styles.sellTextname}>Gutika</Text>

                      <View
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          alignItems: 'center',
                        }}>
                        <Text style={styles.rupeeText}>₹</Text>
                        <Text style={styles.moneyText}>900</Text>
                        <Text style={styles.rupeeTextcut}>₹</Text>
                        <Text style={styles.moneyTextcutted}>900</Text>

                        <Text style={styles.rupeepercentage}>30%OFF</Text>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </ImageBackground>
          <View
            style={{
              height: 5,
              width: 60,
              backgroundColor: '#3F55F6',
              marginTop: 42,
              marginLeft: 15,
            }}></View>
          <Text
            style={{
              color: '#FFF',
              fontSize: 30,
              lineHeight: 45,
              width: 200,
              marginLeft: 15,
              marginTop: 5,
              fontFamily: 'Avenir-Heavy',
              fontWeight: '900',
            }}>
            Be spirited Fearless
          </Text>

          <Text
            style={{
              color: '#FFFFFF80',
              marginLeft: 15,
              fontSize: 15,
              lineHeight: 18,
              fontFamily: 'Avenir-Medium',
              fontWeight: '500',
              marginVertical: 10,
            }}>
            © CALLMYASTRO
          </Text>
        </ScrollView>
      </SafeAreaProvider>
    </MainView>
  );
};

export default Shop;

const styles = StyleSheet.create({
  ftimag: {
    borderRadius: 10,
    marginVertical: 2,
    width: 145,
    height: 200,
    resizeMode: 'contain',
  },
  catseller: {
    borderRadius: 10,
    // marginVertical: 2,
    width: 145,
    height: 200,

    resizeMode: 'contain',
  },
  catImgs: {
    width: 98,
    height: 98,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  catImg: {
    width: 68,
    height: 68,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  catTexts: {
    marginBottom: 10,
    alignSelf: 'center',
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Avenir-Heavy',
    color: '#FFF',
    marginTop: 10,
    fontWeight: '900',
  },
  catText: {
    alignSelf: 'center',
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Avenir-Heavy',
    color: '#FFF',
    marginTop: 10,
    fontWeight: '900',
  },
  catTextss: {
    alignSelf: 'center',
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  sellTextname: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    // marginTop: 7,
    fontWeight: '500',
  },
  rupeeText: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeeTextcut: {
    marginLeft: 5,
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeepercentage: {
    marginTop: 8,
    marginLeft: 5,
    fontSize: 11,
    lineHeight: 15,
    fontFamily: 'Avenir-Medium',
    color: '#3F55F6',
    fontWeight: '500',
  },

  moneyText: {
    fontSize: 20,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  moneyTextcutted: {
    fontSize: 20,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '500',
    textDecorationLine: 'line-through',
  },
});
