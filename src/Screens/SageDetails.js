import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import VideoPlayer from 'react-native-video-player';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
const SageDetails = ({navigation}) => {
  const VideoPlayer = props => (
    <View>
      <ImageBackground
        source={props.videoImage}
        style={styles.videoPlayerImage}>
        <Image source={props.playImage} style={styles.playButton} />
      </ImageBackground>
      <Text style={styles.playerText}>Coming home to yourself</Text>
      <View style={styles.playerView}>
        <Image source={props.adminImage} style={styles.adminImg} />
        <Text style={styles.playerText1}>{props.admin}</Text>
      </View>
    </View>
  );

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <ImageBackground
          source={require('../assets/Rectangle.png')}
          style={styles.bg}>
          <View style={styles.topView}>
            <TouchableOpacity onPress={navigation.goBack}>
              <Image
                source={require('../assets/back.png')}
                style={styles.back}
              />
            </TouchableOpacity>
            <View style={styles.topView_1}>
              <Text style={styles.bgText}>BUY NOW</Text>
              <Image
                source={require('../assets/right-arrow.png')}
                style={styles.back}
              />
            </View>
          </View>

          <Text style={styles.toptext}>Mantras</Text>
          <Text style={styles.bgText_1}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry Lorem Ipsum has been the.{' '}
          </Text>
        </ImageBackground>

        <ImageBackground
          source={require('../assets/q.png')}
          style={styles.middleImage}>
          <Text style={styles.middleText}>Coming home to yourself</Text>
          <Text style={styles.middleText_1}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </Text>
          <Text style={[styles.middleText_1, {marginTop: 10}]}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </Text>
        </ImageBackground>

        <VideoPlayer
          videoImage={require('../assets/s.png')}
          playImage={require('../assets/play.png')}
          adminImage={require('../assets/admin.png')}
          admin="Admin"
        />

        <VideoPlayer
          videoImage={require('../assets/s.png')}
          playImage={require('../assets/play.png')}
          adminImage={require('../assets/admin.png')}
          admin="Admin"
        />
      </ScrollView>
    </MainView>
  );
};

export default SageDetails;

const styles = StyleSheet.create({
  bg: {
    height: height / 2,
    width: '100%',
    // flex: 1,
    resizeMode: 'contain',
  },
  back: {
    width: 12,
    height: 20,

    resizeMode: 'contain',
  },
  topView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: StatusBar.currentHeight + 10,
  },
  bgText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFFFFF',
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: 1,
    marginHorizontal: 10,
  },
  topView_1: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  toptext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 70,
  },
  bgText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginHorizontal: 50,
    textAlign: 'center',
    opacity: 0.9,
    marginTop: 20,
    lineHeight: 15,
  },
  middleImage: {
    width: 337,
    height: 262,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: -10,
  },
  middleText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 20,
    marginVertical: 120,
  },
  middleText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 20,
    marginTop: -110,
    marginLeft: 30,
  },
  videoPlayerImage: {
    width: 337,
    height: 166,
    resizeMode: 'contain',
    marginTop: 20,
    alignSelf: 'center',
    // alignItems: 'center',
  },
  playButton: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 60,
  },
  playerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 10,
    marginTop: 10,
  },
  playerView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  playerText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 5,
  },
  adminImg: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
});
