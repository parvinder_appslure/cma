import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import Carousel from 'react-native-banner-carousel';

const BannerWidth = Dimensions.get('window').width;

const AddToCart = ({navigation}) => {
  const [banner, setBanner] = useState([
    {
      key: 1,
      // source: require('../assets/3.png'),
    },
    {
      key: 2,
      // source: require('../assets/3.png'),
    },
    {
      key: 3,
      // source: require('../assets/3.png'),
    },
    {
      key: 4,
      // source: require('../assets/3.png'),
    },
  ]);

  const renderPage = (image, index) => {
    //    alert(JSON.stringify(image))
    return (
      <View key={index} style={{width: Dimensions.get('window').width}}>
        <Image
          style={{
            width: Dimensions.get('window').width,
            height: 492,
            resizeMode: 'stretch',
          }}
          source={require('../assets/3.png')}
        />
      </View>
    );
  };
  return (
    <MainView>
      <View
        style={{
          height: 54,
          width: Dimensions.get('window').width,
          marginTop: 20,
          flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <View
          style={{
            width: '83%',
            flexDirection: 'row',
            marginLeft: 20,
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={{height: 22, width: 12, resizeMode: 'contain'}}
              source={require('../assets/back.png')}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity>
          <View style={{alignSelf: 'flex-end'}}>
            <Image
              style={{height: 24, width: 25, resizeMode: 'contain'}}
              source={require('../assets/cart.png')}
            />
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView>
        <View style={styles.container}>
          <Carousel
            autoplay
            autoplayTimeout={5000}
            loop
            pageIndicatorStyle={{
              height: 7,
              width: 7,
              borderRadius: 3.5,
              backgroundColor: '#FFFFFF',
            }}
            pageIndicatorContainerStyle={{position: 'absolute', bottom: 10}}
            activePageIndicatorStyle={{
              height: 7,
              width: 8,
              borderRadius: 4,
              backgroundColor: '#3F55F6',
            }}
            index={0}
            pageSize={BannerWidth}>
            {banner.map((image, index) => renderPage(image, index))}
          </Carousel>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 16,
              lineHeight: 20,
              fontFamily: 'Avenir-Medium',
              color: '#FFF',
              fontWeight: '400',
            }}>
            Decoration
          </Text>
          <Text
            style={{
              fontSize: 29,
              lineHeight: 29,
              fontFamily: 'Avenir-Heavy',
              color: '#FFF',
              fontWeight: '900',
            }}>
            ₹799
          </Text>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 29,
              lineHeight: 29,
              fontFamily: 'Avenir-Heavy',
              color: '#FFF',
              fontWeight: '900',
            }}>
            Fengshui
          </Text>

          <Text
            style={{
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Heavy',
              color: '#FFFFFF50',
              fontWeight: '900',
              textDecorationLine: 'line-through',
            }}>
            ₹900
          </Text>
        </View>

        <Text
          style={{
            fontSize: 12,
            lineHeight: 20,
            marginTop: 10,
            marginLeft: 20,
            fontFamily: 'Avenir-Medium',
            color: '#FFF',
            fontWeight: '400',
          }}>
          Product code: 12JWHAMI97T
        </Text>

        <TouchableOpacity
          onPress={() => navigation.navigate('Productdetails')}
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 40,
            marginBottom: 40,
            justifyContent: 'center',
            backgroundColor: '#3F55F6',
            borderRadius: 10,
          }}>
          <Text
            style={{
              alignSelf: 'center',
              marginVertical: 15,
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Medium',
              color: '#FFF',
              fontWeight: '500',
            }}>
            Add To Cart
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    height: 492,
  },
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
    marginLeft: 10,
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 20,
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
  },

  bottomText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
  },

  walletImage: {
    width: 30,
    height: 33,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
  historyview: {
    flexDirection: 'row',
    marginLeft: -10,
    alignItems: 'center',
    marginTop: 20,
  },
  amount: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#7ED321',
    marginHorizontal: 40,
  },
  historyview1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    marginTop: 20,
    opacity: 0.1,
    // marginHorizontal: 10,
  },
  amount1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FF001F',
    marginHorizontal: 20,
  },
});

export default AddToCart;
