import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FilledTextField, TextField} from 'react-native-material-textfield';
import {color} from 'react-native-reanimated';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {
  BottomButton,
  CustomInputField,
  Header,
  MainView,
} from '../Custom/CustomView';

const Support = ({navigation}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    orderNumber: '',
    query: '',
  });

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header onPress={navigation.goBack} title={'Support'} />

        <Image
          source={require('../assets/question.png')}
          style={styles.topimage}
        />
        <Text style={styles.toptext}>Need Some Help?</Text>

        <View style={styles.middleView}>
          <View style={styles.box}>
            <Image
              style={styles.middleimage}
              source={require('../assets/envelopes.png')}
            />
            <View style={styles.middlebox1}>
              <Text style={styles.boxText}>Email</Text>
              <Text style={[styles.boxText, {opacity: 1}]}>
                support@callmy.com
              </Text>
            </View>
          </View>
        </View>

        <View style={[styles.middleView, {marginTop: 30}]}>
          <View style={styles.box}>
            <Image
              style={styles.bottomimage}
              source={require('../assets/help.png')}
            />
            <Text style={styles.bottomText}>Get in Touch</Text>
          </View>
          <Text style={styles.bottomText1}>
            Please give us in between 12 to 24 working hours to address your
            issues.
          </Text>

          <FilledTextField
            label="Name"
            fontSize={18}
            textColor={'#FFFFFF'}
            tintColor={'#FFFFFF'}
            baseColor="#FFFFFF"
            labelFontSize={16}
            containerStyle={styles.containerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            lineWidth={0}
            activeLineWidth={0}
          />
          <FilledTextField
            label="Email"
            fontSize={18}
            textColor={'#FFFFFF'}
            tintColor={'#FFFFFF'}
            baseColor="#FFFFFF"
            labelFontSize={16}
            containerStyle={styles.containerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            lineWidth={0}
            activeLineWidth={0}
          />

          <FilledTextField
            label="Message"
            fontSize={18}
            multiline={true}
            textColor={'#FFFFFF'}
            tintColor={'#FFFFFF'}
            baseColor="#FFFFFF"
            labelFontSize={16}
            containerStyle={styles.containerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            lineWidth={0}
            activeLineWidth={0}
          />
        </View>

        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.bottomView}
          onPress={navigation.goBack}>
          <Text style={styles.textTitle}>Send</Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

export default Support;

const styles = StyleSheet.create({
  topimage: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 30,
  },
  toptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#FFFFFF',
    textAlign: 'center',
    marginTop: 10,
  },
  middleView: {
    backgroundColor: '#13042A',
    padding: 20,
    marginTop: 70,
    marginHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    borderRadius: 13,
    marginBottom: 10,
  },
  middleimage: {
    width: 26,
    height: 26,
    resizeMode: 'contain',
  },
  box: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  middlebox1: {
    marginHorizontal: 20,
  },
  boxText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.7,
  },
  bottomimage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.7,
    marginLeft: 20,
  },
  bottomText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    marginTop: 20,
  },
  containerStyle: {
    width: '100%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 40,
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    backgroundColor: '#1B172C',
  },
  bottomView: {
    width: '88%',
    alignSelf: 'center',
    backgroundColor: '#3F55F6',
    borderRadius: 5,
    padding: 12,
    marginTop: 50,
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
