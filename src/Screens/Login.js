import CheckBox from '@react-native-community/checkbox';
import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  Image,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
import Loader from '../component/Loader';
import {Api} from '../services/Api';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {ButtonStyle, CustomInputField, MainView} from '../Custom/CustomView';

const Login = ({navigation}) => {
  const [state, setState] = useState({
    phone: '9896449941',
    flag: true,
    isLoading: false,
  });
  const toggleLoader = isLoading => setState({...state, isLoading});
  const onContinueHandler = async () => {
    const {phone, flag} = state;
    if (!flag) {
      alert('Please check the Privacy Policy');
      return;
    }
    if (phone.length === 10 && !isNaN(phone)) {
      toggleLoader(true);
      const response = await Api.otp({phone});
      toggleLoader(false);
      const {status = false, data = {}} = response;
      if (status) {
        navigation.navigate('Otp', {
          phone,
          data,
        });
      } else {
        alert('Something went wrong');
      }
    } else {
      alert('Please enter valid number');
    }
  };

  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.isLoading} />
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.replace('TabNavigator')}>
          <Text style={styles.topText}>SKIP</Text>
        </TouchableOpacity>
        <Text style={styles.textStyles1}>LOGIN</Text>
        <Text style={styles.OtpText}>Welcome Back!</Text>
        <CustomInputField
          label="Mobile Number"
          keyboardType={'phone-pad'}
          maxLength={10}
          defaultValue={state.phone}
          onChangeText={phone => setState({...state, phone})}
        />

        <View style={styles.middleview}>
          <CheckBox
            disabled={false}
            value={state.flag}
            onValueChange={flag => setState({...state, flag})}
            tintColors={{
              true: '#3F55F6',
              false: '#FFFFFF',
            }}
          />
          <Text style={styles.textStyles2}>
            By Clicking Continue , you agree to
            <Text style={styles.middleText}> Our Terms </Text>
            and <Text style={styles.middleText}> Privacy Policy.</Text>
          </Text>
        </View>
        {ButtonStyle('LOG IN', '#3F55F6', '#FFFFFF', onContinueHandler)}

        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
          <Text
            style={[
              styles.textStyles2,
              {alignSelf: 'center', marginTop: 20, fontSize: 15},
            ]}>
            Don't have an account?
            <Text style={styles.middleText}> Sign Up Now </Text>
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

export default Login;

const styles = StyleSheet.create({
  imageStyles: {
    height: 240,
    width: 194,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  textStyles1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 72,
    fontWeight: '600',
    color: '#6F6F7B',
    marginTop: 30,
    opacity: 0.3,
  },
  OtpText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 24,
    fontWeight: '600',
    color: '#FFFFFF',
    marginHorizontal: 20,
    bottom: 32,
  },
  textStyles2: {
    fontFamily: 'Avenir',
    fontSize: 13,
    fontWeight: '500',
    color: '#FFFFFF',
    marginHorizontal: 20,
    // opacity: 0.5,
    lineHeight: 20,
  },

  otpInput: {
    width: '84%',
    height: 60,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#1B172C',
    backgroundColor: '#1B172C',
    borderRadius: 30,
    fontSize: 35,
    color: '#ffffff',
  },
  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#1B172C',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#1B172C',
  },
  topText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    alignSelf: 'flex-end',
    marginHorizontal: 30,
    marginTop: 40,
  },
  middleview: {
    flexDirection: 'row',
    marginHorizontal: 20,
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '600',
    color: '#6266F9',
  },
});
