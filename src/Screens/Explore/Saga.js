import React, {memo} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Heading from './Heading';

const Saga = memo(() => {
  const navigation = useNavigation();
  return (
    <View>
      <Heading title={'Saga'} flag={false} onPress={() => {}} />
      <View>
        <FlatList
          data={sageData}
          horizontal={true}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => navigation.navigate(item.nav)}>
                <ImageBackground
                  source={item.bgsource}
                  style={styles.SagebgImage}>
                  <Text style={styles.sageText}>{item.title}</Text>

                  <View style={styles.touchbox}>
                    <Image
                      source={require('../../assets/dot.png')}
                      style={styles.sageDotSource}
                    />
                    <Text style={styles.sageText1}>Explore</Text>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  SagebgImage: {
    width: 155,
    height: 200,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 20,
    // marginLeft:
  },
  sageDotSource: {
    width: 11,
    height: 11,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },

  sageText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 60,
    textAlign: 'center',
  },
  sageText1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    // marginTop: 10,
    alignSelf: 'center',
  },
});

export default Saga;

const sageData = [
  {
    key: 1,
    bgsource: require('../../assets/1.png'),
    title: 'Mantras',
    nav: 'SageDetails',
  },
  {
    key: 2,
    bgsource: require('../../assets/2.png'),
    title: 'Earthling',
    nav: 'SageDetails',
  },
  {
    key: 3,
    bgsource: require('../../assets/3.png'),
    title: 'Moon & Astrology',
    nav: 'SageDetails',
  },
  {
    key: 4,
    bgsource: require('../../assets/4.png'),
    title: 'Chromotherapy',
    nav: 'SageDetails',
  },
  {
    key: 5,
    bgsource: require('../../assets/5.png'),
    title: 'Eclipses',
    nav: 'SageDetails',
  },
  {
    key: 6,
    bgsource: require('../../assets/6.png'),
    title: 'Moon Calendar',
    nav: 'SageDetails',
  },
];
