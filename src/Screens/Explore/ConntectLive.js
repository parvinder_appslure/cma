import React, {useEffect, memo, useContext} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Heading from './Heading';
import {SocketContext} from '../../redux/context';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/core';
import * as actions from '../../redux/actions';
import {width} from '../../services/Api';
const ConnectLive = memo(() => {
  const {onlineAstrologer} = useSelector(store => store);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const socket = useContext(SocketContext);
  useEffect(() => {
    socket.on('online_astrologers', response => {
      // consolejson(response);
      console.log('socket response :: online astrologers');
      const {status = false, data = []} = response;
      console.log(data.length);
      if (status) {
        dispatch(actions.OnlineAstrologer(data));
      }
    });
    socket.emit('online_astrologers');
  }, []);
  return onlineAstrologer.length ? (
    <>
      <Heading
        title={'Connect Live'}
        flag={onlineAstrologer.length > 3}
        onPress={() => navigation.navigate('OnlineAstrologer')}
      />
      <View style={styles.container}>
        <Astrologer item={onlineAstrologer?.[0] || {}} />
        <Astrologer item={onlineAstrologer?.[1] || {}} />
        <Astrologer item={onlineAstrologer?.[2] || {}} />
        <Astrologer item={onlineAstrologer?.[3] || {}} />
      </View>
    </>
  ) : null;
});
const Astrologer = memo(({item = {}, onPress}) => {
  const {imageUrl, name, expertise} = item;
  consolejson(item);
  return (
    Object.keys(item).length !== 0 && (
      <TouchableOpacity onPress={onPress} style={styles.viewAstrologer}>
        <View style={styles.viewTop}>
          <LinearGradient
            colors={['#F7B500', '#FF6469']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={styles.linearGradient}>
            <Image
              source={require('../../assets/like.png')}
              style={styles.imageLike}
            />
            <Text style={styles.textChoice}>Most Choice</Text>
          </LinearGradient>
          <View style={styles.viewOnline} />
        </View>
        <View style={styles.viewBottom}>
          <View style={{flex: 1, marginRight: 5}}>
            <Text style={styles.textName}>{name}</Text>
            <Text style={styles.textTitle}>{expertise}</Text>
          </View>
          <Image source={{uri: imageUrl}} style={styles.imageAstro} />
        </View>
      </TouchableOpacity>
    )
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 8,
    flexWrap: 'wrap',
  },
  viewAstrologer: {
    width: width / 2 - 24,
    margin: 8,
    backgroundColor: '#13042A',
    borderWidth: 1,
    borderColor: '#FFFFFF33',
    borderRadius: 10,
    overflow: 'hidden',
  },
  viewTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  viewBottom: {
    flexDirection: 'row',
    marginLeft: 10,
    justifyContent: 'space-between',
  },
  viewOnline: {
    width: 10,
    height: 10,
    backgroundColor: '#6DD400',
    padding: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  linearGradient: {
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 4,
  },
  imageLike: {
    width: 8,
    height: 8,
    marginRight: 5,
    resizeMode: 'contain',
  },
  textChoice: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 8,
    color: '#FFFFFF',
  },
  imageAstro: {
    width: 70,
    height: 90,
  },
  textName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 13,
    color: '#FFFFFF',
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 9,
    color: '#FFFFFF',
  },
});

export default ConnectLive;
