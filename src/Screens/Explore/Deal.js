import {useNavigation} from '@react-navigation/core';
import React, {memo} from 'react';
import {
  FlatList,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';

const Deal = memo(() => {
  const navigation = useNavigation();
  return (
    <View>
      <Heading title={`Deal's Today`} flag={true} onPress={() => {}} />
      <View>
        <FlatList
          data={people}
          numColumns={2}
          contentContainerStyle={{
            alignItems: 'center',
          }}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => navigation.navigate(item.nav)}>
                <ImageBackground style={styles.ib} source={item.source}>
                  <Text style={styles.igText}>{item.title}</Text>
                  <Text style={styles.igText1}>{item.price}</Text>
                </ImageBackground>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
});

export default Deal;
const styles = StyleSheet.create({
  ib: {
    width: 172,
    height: 180,
    resizeMode: 'contain',
    margin: 5,
    // marginLeft: 10,
    marginHorizontal: 5,
  },
  igText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '400',
    alignSelf: 'center',
    color: '#FFFFFF',
    marginTop: 110,
    opacity: 0.8,
    textAlign: 'center',
    marginHorizontal: 10,
    // backgroundColor: 'yellow',
  },
  igText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#FFFFFF',
    // marginTop: 110,
  },
});

const people = [
  {
    key: 1,
    source: require('../../assets/chat.png'),
    title: 'Pukhraj',
    price: '₹ 11999',
    nav: 'Profile',
  },
  {
    key: 2,
    source: require('../../assets/chat.png'),
    title: 'Recharge',
    price: '200 get  250',
    nav: 'Profile',
  },
  {
    key: 3,
    source: require('../../assets/chat.png'),
    title: 'Fengshui',
    price: '799',
    nav: 'Profile',
  },
  {
    key: 4,
    source: require('../../assets/chat.png'),
    title: 'On your 1st Video Consultation',
    price: '50%',
    nav: 'Profile',
  },
];
