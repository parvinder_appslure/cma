import React, {memo} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';

const Puja = memo(() => {
  return (
    <View>
      <Heading title={'Online Puja'} flag={false} onPress={() => {}} />
      <ScrollView contentContainerStyle={styles.scrollContainer} horizontal>
        {data.map(item => {
          return (
            <TouchableOpacity style={styles.touch}>
              <Image
                source={require('../../assets/horo.png')}
                style={styles.image}
              />
              <Text style={styles.textName}>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
});

export default Puja;

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 5,
  },
  touch: {
    alignItems: 'center',
    marginHorizontal: 5,
  },
});

const data = [
  {name: 'Aries', id: 1},
  {name: 'Aries', id: 2},
  {name: 'Aries', id: 3},
  {name: 'Aries', id: 4},
  {name: 'Aries', id: 5},
  {name: 'Aries', id: 6},
  {name: 'Aries', id: 7},
  {name: 'Aries', id: 8},
  {name: 'Aries', id: 9},
  {name: 'Aries', id: 10},
  {name: 'Aries', id: 11},
  {name: 'Aries', id: 12},
];
