import React, {memo} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';

import {useNavigation} from '@react-navigation/native';
const Horoscope = memo(() => {
  const navigation = useNavigation();
  return (
    <View>
      <Heading title={'Daily Horoscope'} flag={false} onPress={() => {}} />
      <ScrollView contentContainerStyle={styles.scrollContainer} horizontal>
        {horoscope.map(item => {
          return (
            <TouchableOpacity
              style={styles.touch}
              onPress={() => navigation.navigate('HoroscopeDetail', item)}>
              <Image source={item?.source} style={styles.image} />
              <Text style={styles.textName}>{item.title}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
});

export default Horoscope;

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 5,
  },
  touch: {
    alignItems: 'center',
    marginHorizontal: 5,
  },
  image: {
    width: 70,
    height: 70,
    marginBottom: 5,
  },
  textName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});

const horoscope = [
  {
    id: 'aries',
    title: 'Aries',
    period: 'March 21-April 20',
    source: require('../../assets/horoscopeimages/aries.png'),
  },
  {
    id: 'taurus',
    title: 'Taurus',
    period: 'April 21-May 20',
    source: require('../../assets/horoscopeimages/tarus.png'),
  },
  {
    id: 'gemini',
    title: 'Gemini',
    period: 'May 21-Jun 21',
    source: require('../../assets/horoscopeimages/gemini.png'),
  },
  {
    id: 'cancer',
    title: 'Cancer',
    period: 'Jun 22-July 22',
    source: require('../../assets/horoscopeimages/cancer.png'),
  },
  {
    id: 'leo',
    title: 'Leo',
    period: 'July 23-August 23',
    source: require('../../assets/horoscopeimages/leo.png'),
  },
  {
    id: 'virgo',
    title: 'Virgo',
    period: 'August 24-September 23',
    source: require('../../assets/horoscopeimages/vigro.png'),
  },
  {
    id: 'libra',
    title: 'Libra',
    period: 'September 24-October 23',
    source: require('../../assets/horoscopeimages/libra.png'),
  },
  {
    id: 'scorpio',
    title: 'Scorpio',
    period: 'October 24-November 22',
    source: require('../../assets/horoscopeimages/scorpio.png'),
  },
  {
    id: 'sagittarius',
    title: 'Sagittarius',
    period: 'November 23-December 21',
    source: require('../../assets/horoscopeimages/sagitt.png'),
  },
  {
    id: 'capricorn',
    title: 'Capricorn',
    period: 'December 22-January 21',
    source: require('../../assets/horoscopeimages/cap.png'),
  },
  {
    id: 'aquarius',
    title: 'Aquarius',
    period: 'January 22-February 19',
    source: require('../../assets/horoscopeimages/aquar.png'),
  },
  {
    id: 'pisces',
    title: 'Pisces',
    period: 'February 20-March 20',
    source: require('../../assets/horoscopeimages/pisces.png'),
  },
];
