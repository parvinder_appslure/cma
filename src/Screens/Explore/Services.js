import React, {memo} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Heading from './Heading';
import {useNavigation} from '@react-navigation/native';
const Services = memo(() => {
  const navigation = useNavigation();

  const TouchBox = props => (
    <View style={{marginLeft: 5}}>
      <TouchableOpacity onPress={props.onPress} activeOpacity={0.8}>
        <ImageBackground style={styles.bg} source={props.source}>
          <Text style={styles.touchText}>{props.title}</Text>
        </ImageBackground>
      </TouchableOpacity>
      <View style={{width: '55%', marginTop: 10, alignSelf: 'center'}}>
        <Text style={styles.tbtext}>{props.tbtext}</Text>
      </View>
    </View>
  );
  return (
    <View>
      <Heading title={'Our Services'} flag={true} onPress={() => {}} />
      <View style={styles.touchView}>
        <TouchBox
          onPress={() => navigation.navigate('ChatWithAstrologer')}
          source={require('../../assets/chat.png')}
          title="Chat"
          tbtext="Chat with astrologer"
        />
        <TouchBox
          onPress={() => navigation.navigate('ChatWithAstrologer')}
          source={require('../../assets/talk-bg.png')}
          title="Talk"
          tbtext="Talk with astrologer"
        />
        <TouchBox
          onPress={() => navigation.navigate('ChatWithAstrologer')}
          source={require('../../assets/video-bg.png')}
          title="Video"
          tbtext="Video with astrologer"
        />
      </View>
    </View>
  );
});
const styles = StyleSheet.create({
  touchView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    // marginHorizontal: 4,
  },
  bg: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },

  touchText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 4,
    alignSelf: 'center',
  },
  tbtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    // marginHorizontal: 10,
  },
});
export default Services;
