import React, {useState} from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import MaterialTabs from 'react-native-material-tabs';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const BookingHistory = ({navigation}) => {
  const [selectedTab, setSelectedTab] = useState(0);

  const [person, setPerson] = useState([
    {
      key: 1,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
    {
      key: 2,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
    {
      key: 3,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
    {
      key: 4,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
    {
      key: 5,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
    {
      key: 6,
      source: require('../assets/dp.png'),
      name: 'Vipul Pandey',
      date: 'Date : 04 Dec 20, 03:40 PM',
      rate: 'Rate : ₹10/min',
      duration: 'Duration : 9 mins',
      deduction: 'Dedution : ₹90/-',
      status: 'Status : ',
      status1: 'Completed',
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={'Booking History'} />
      <View style={styles.container}>
        <MaterialTabs
          items={['Chat', 'Call', 'Video Call', 'Report']}
          selectedIndex={selectedTab}
          onChange={setSelectedTab}
          barColor="#13042A"
          indicatorColor="#6266F9"
          activeTextColor="#6266F9"
          inactiveTextColor="#FFFFFF"
          scrollable={true}
          textStyle={{fontSize: 16}}
        />

        <FlatList
          data={person}
          numColumns={1}
          renderItem={({item}) => {
            return (
              <View style={styles.ftbox}>
                <View style={styles.ftbox1}>
                  <Image style={styles.ftimage} source={item.source} />
                  <View style={styles.ftbox2}>
                    <Text style={styles.fttext}>{item.name}</Text>
                    <Text style={styles.fttext1}>{item.date}</Text>
                    <Text style={styles.fttext1}>{item.rate}</Text>
                    <Text style={styles.fttext1}>{item.duration}</Text>

                    <Text style={styles.fttext1}>
                      {item.status}
                      <Text style={[styles.fttext1, {color: '#00C327'}]}>
                        {item.status1}
                      </Text>
                    </Text>
                  </View>
                  <Image
                    style={styles.ftimagebt}
                    source={require('../assets/message.png')}
                  />
                </View>
              </View>
            );
          }}
        />
      </View>
    </MainView>
  );
};

export default BookingHistory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ftbox: {
    marginHorizontal: 20,
    marginTop: 10,
    marginTop: 10,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#13042A',
  },
  ftimage: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    marginTop: 5,
  },
  ftimagebt: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
  ftbox1: {
    flexDirection: 'row',
  },
  fttext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  fttext1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 3,
  },
  ftbox2: {
    marginHorizontal: 10,
  },
});
