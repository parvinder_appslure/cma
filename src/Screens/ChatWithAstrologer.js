import React, {useState} from 'react';
import {
  FlatList,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';

const ChatWithAstrologer = ({navigation}) => {
  const [people, setPeople] = useState([
    {
      key: 1,
      onlinesource: require('../assets/online.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astrologer.png'),
      nav: 'AstrologerDetail',
    },
    {
      key: 2,
      onlinesource: require('../assets/busy.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astro1.png'),
    },
    {
      key: 3,
      onlinesource: require('../assets/offline.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astrologer.png'),
    },
    {
      key: 4,
      onlinesource: require('../assets/online.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astrologer.png'),
    },
    {
      key: 5,
      onlinesource: require('../assets/online.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astrologer.png'),
    },
    {
      key: 6,
      onlinesource: require('../assets/online.png'),
      name: 'Vipul Pandey',
      title: 'Vedic Astrology',
      starsource: require('../assets/star.png'),
      exp: '8 Years',
      lan: 'English...',
      price: '₹ 300/min',
      profilesource: require('../assets/astrologer.png'),
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <View style={styles.topview}>
        <View style={styles.topview1}>
          <TouchableOpacity onPress={navigation.goBack}>
            <Image
              style={styles.arrow}
              source={require('../assets/back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.toptext}>Chat with astrologer</Text>
        </View>
        <Image
          style={styles.filter}
          source={require('../assets/filter2.png')}
        />
      </View>

      <FlatList
        data={people}
        numColumns={2}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate(item.nav)}>
              <View style={styles.bgView}>
                <Image style={styles.statusimage} source={item.onlinesource} />
                <View style={styles.topview1}>
                  <View style={styles.topview2}>
                    <Text style={styles.ftname}>{item.name}</Text>
                    <Text
                      style={[
                        styles.ftname,
                        {fontSize: 10, marginTop: 5, opacity: 0.8},
                      ]}>
                      {item.title}
                    </Text>
                    <View style={styles.topview1}>
                      <Image
                        style={styles.starimage}
                        source={item.starsource}
                      />
                      <Image
                        style={styles.starimage}
                        source={item.starsource}
                      />
                      <Image
                        style={styles.starimage}
                        source={item.starsource}
                      />
                      <Image
                        style={styles.starimage}
                        source={item.starsource}
                      />
                    </View>
                    <Text style={styles.ftExperince}>Experience</Text>
                    <Text style={styles.ftExp}>{item.exp}</Text>
                    <Text style={styles.ftExperince}>Language</Text>
                    <Text style={styles.ftExp}>{item.lan}</Text>
                    <Text style={styles.ftExperince}>Price</Text>
                    <Text style={styles.ftExp}>{item.price}</Text>
                  </View>
                  <Image
                    source={item.profilesource}
                    style={styles.profileImage}
                  />
                </View>

                <View style={styles.bottomview}>
                  <Image
                    source={require('../assets/chatting.png')}
                    style={styles.bottomImg}
                  />
                  <View style={styles.line} />
                  <Image
                    source={require('../assets/telephone.png')}
                    style={styles.bottomImg}
                  />

                  <View style={styles.line} />
                  <Image
                    source={require('../assets/video-camera2.png')}
                    style={styles.bottomImg}
                  />
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </MainView>
  );
};

export default ChatWithAstrologer;

const styles = StyleSheet.create({
  arrow: {
    width: 12,
    height: 20,
    resizeMode: 'contain',
  },
  topview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  toptext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 10,
  },
  filter: {
    width: 20,
    height: 18,
    resizeMode: 'contain',
  },
  topview1: {
    flexDirection: 'row',
    // alignItems: 'center',
  },
  bgView: {
    backgroundColor: '#13042A',
    padding: 5,
    // marginLeft: 5,
    marginHorizontal: 5,
    marginTop: 20,

    borderColor: '#677294',
    borderWidth: 1,
    borderRadius: 10,
  },
  statusimage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
    // left: -20
    marginHorizontal: 10,
    // backgroundColor: 'yellow',
  },
  ftname: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFFFFF',
    // marginHorizontal: 10,
  },
  profileImage: {
    width: 90,
    height: 139,
    resizeMode: 'contain',
    // marginRight: 10
    marginLeft: -25,
    left: 10,
    marginHorizontal: 5,
  },
  topview2: {
    left: 10,
  },
  starimage: {
    width: 8,
    height: 8,
    resizeMode: 'contain',
    marginTop: 5,
  },
  ftExp: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#FFFFFF',
    // marginHorizontal: 10,
  },
  ftExperince: {
    fontFamily: 'Avenir-Medium',
    fontSize: 10,
    fontWeight: '400',
    color: '#677294',
    // marginHorizontal: 10,
  },
  bottomImg: {
    width: 18,
    height: 16,
    resizeMode: 'contain',
  },
  bottomview: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-around',
    marginTop: 10,
    marginBottom: 10,
  },
  line: {
    height: '120%',
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    marginHorizontal: 10,
    opacity: 0.5,
  },
});
