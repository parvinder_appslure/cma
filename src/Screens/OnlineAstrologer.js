import React, {memo} from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useSelector} from 'react-redux';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {width} from '../services/Api';
const OnlineAstrologer = ({navigation, route}) => {
  const {onlineAstrologer} = useSelector(store => store);
  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={'Online Astrologer'} />
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        {onlineAstrologer.map(item => {
          return <Astrologer item={item} />;
        })}
      </ScrollView>
    </MainView>
  );
};

const Astrologer = memo(({item = {}, onPress}) => {
  const {imageUrl, name, expertise} = item;
  return (
    Object.keys(item).length !== 0 && (
      <TouchableOpacity onPress={onPress} style={styles.viewAstrologer}>
        <View style={styles.viewTop}>
          <LinearGradient
            colors={['#F7B500', '#FF6469']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={styles.linearGradient}>
            <Image
              source={require('../assets/like.png')}
              style={styles.imageLike}
            />
            <Text style={styles.textChoice}>Most Choice</Text>
          </LinearGradient>
          <View style={styles.viewOnline} />
        </View>
        <View style={styles.viewBottom}>
          <View style={{flex: 1, marginRight: 5}}>
            <Text style={styles.textName}>{name}</Text>
            <Text style={styles.textTitle}>{expertise}</Text>
          </View>
          <Image source={{uri: imageUrl}} style={styles.imageAstro} />
        </View>
      </TouchableOpacity>
    )
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 8,
    flexWrap: 'wrap',
  },
  contentContainerStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexGrow: 1,
    margin: 8,
  },
  viewAstrologer: {
    width: width / 2 - 24,
    margin: 8,
    backgroundColor: '#13042A',
    borderWidth: 1,
    borderColor: '#FFFFFF33',
    borderRadius: 10,
    overflow: 'hidden',
  },
  viewTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  viewBottom: {
    flexDirection: 'row',
    marginLeft: 10,
    justifyContent: 'space-between',
  },
  viewOnline: {
    width: 10,
    height: 10,
    backgroundColor: '#6DD400',
    padding: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  linearGradient: {
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 4,
  },
  imageLike: {
    width: 8,
    height: 8,
    marginRight: 5,
    resizeMode: 'contain',
  },
  textChoice: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 8,
    color: '#FFFFFF',
  },
  imageAstro: {
    width: 70,
    height: 90,
  },
  textName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 13,
    color: '#FFFFFF',
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 9,
    color: '#FFFFFF',
  },
});
export default OnlineAstrologer;
