import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {Api} from '../services/Api';

const Wallet = ({navigation}) => {
  const {userDetail} = useSelector(store => store);
  const [state, setState] = useState({
    data: [],
  });
  useEffect(() => {
    (async () => {
      const response = await Api.walletHistory({
        limit: 10,
        offset: 0,
      });
      const {
        status = false,
        message = 'Something went wrong',
        data = [],
      } = response;
      setState({...state, data});
    })();
  }, []);
  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={'Wallet'} />
      <View style={styles.topView}>
        <View style={styles.marginView}>
          <Text style={styles.topText}>AVAILABLE BALANCE</Text>
          <View style={styles.topView_1}>
            <Image
              style={styles.topimage}
              source={require('../assets/wallet.png')}
            />
            <Text
              style={styles.topText_1}>{`\u20b9 ${userDetail?.wallet}`}</Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Recharge')}>
            <Text style={styles.topText_2}>Recharge wallet</Text>
          </TouchableOpacity>
        </View>
      </View>

      <Text style={styles.middleText}>Wallet History</Text>
      {state.data.length !== 0 && (
        <FlatList
          data={state.data}
          keyExtractor={item => item.id.toString()}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            const {id, txn_amount, created_at, txn_name, type} = item;
            consolejson({});
            return <Text>asdf</Text>;
          }}
        />
      )}
      <View style={styles.historyview}>
        <Image
          source={require('../assets/wallet2.png')}
          style={styles.walletImage}
        />

        <View>
          <View style={styles.historyview1}>
            <Text style={styles.bottomText}>Money Added to wallet </Text>
            <Text style={styles.amount}>₹500.00</Text>
          </View>
          <Text style={styles.bottomText_2}>17 Aug, 07:36 pm</Text>
        </View>
      </View>

      <View style={styles.line} />

      <View style={styles.historyview}>
        <Image
          source={require('../assets/wallet2.png')}
          style={styles.walletImage}
        />

        <View>
          <View style={styles.historyview1}>
            <View
              style={{
                width: '60%',
                marginTop: 15,
              }}>
              <Text style={styles.bottomText}>
                Paid to Video Call Service Acharya Ravi (15 min)
              </Text>
            </View>
            <Text style={styles.amount1}>₹500.00</Text>
          </View>
          <Text style={styles.bottomText_2}>17 Aug, 07:36 pm</Text>
        </View>
      </View>
    </MainView>
  );
};

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
    marginLeft: 10,
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },
  middleText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    margin: 20,
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
  },

  bottomText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
  },

  walletImage: {
    width: 30,
    height: 33,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
  historyview: {
    flexDirection: 'row',
    marginLeft: -10,
    alignItems: 'center',
    marginTop: 20,
  },
  amount: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#7ED321',
    marginHorizontal: 40,
  },
  historyview1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    marginTop: 20,
    opacity: 0.1,
    // marginHorizontal: 10,
  },
  amount1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FF001F',
    marginHorizontal: 20,
  },
});

export default Wallet;
