import React, {useState, useEffect} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {MainView, HeaderView, TextError, Header} from '../Custom/CustomView';
import Loader from '../component/Loader';
import {Api} from '../services/Api';
const HoroscopeDetail = ({navigation, route}) => {
  const [state, setState] = useState({
    ...route.params,
    prediction: {},
    isLoading: true,
    error: '',
  });
  useEffect(() => {
    (async () => {
      const response = await Api.dailyPrediction({zodiacSign: state.id});
      const {
        status = false,
        responseData: {prediction = {}},
      } = response;
      setState({
        ...state,
        prediction,
        isLoading: false,
        error: status ? '' : 'Something went wrong',
      });
    })();
  }, []);
  return (
    <MainView>
      <Loader status={state.isLoading} />
      <Header onPress={navigation.goBack} title={'Horoscope Detail'} />
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.viewHeader}>
          <Image source={state.source} style={styles.imageIcon} />
          <View style={styles.viewRight}>
            <Text style={styles.textTitle}>{state.title}</Text>
            <Text style={styles.textDetail}>{state.period}</Text>
          </View>
        </View>
        <PredictionView data={state.prediction} />
        <TextError title={state.error} />
      </ScrollView>
    </MainView>
  );
};

export default HoroscopeDetail;

const PredictionView = ({data}) => {
  return Object.keys(data).length !== 0 ? (
    <View style={styles.viewPrediction}>
      <View style={{marginVertical: 5}}>
        <Title title={'Personal Life'} />
        <Detail title={data.personal_life} />
      </View>
      <View style={{marginVertical: 5}}>
        <Title title={'Profession'} />
        <Detail title={data.profession} />
      </View>
      <View style={{marginVertical: 5}}>
        <Title title={'Health'} />
        <Detail title={data.health} />
      </View>
      <View style={{marginVertical: 5}}>
        <Title title={'Travel'} />
        <Detail title={data.travel} />
      </View>
      <View style={{marginVertical: 5}}>
        <Title title={'Luck'} />
        <Detail title={data.luck} />
      </View>
      <View style={{marginVertical: 5}}>
        <Title title={'Emotions'} />
        <Detail title={data.emotions} />
      </View>
    </View>
  ) : null;
};

const Title = ({title}) => <Text style={styles.textTitle}>{title}</Text>;
const Detail = ({title}) => <Text style={styles.textDetail}>{title}</Text>;

const styles = StyleSheet.create({
  viewHeader: {
    flexDirection: 'row',
    margin: 20,
    alignItems: 'center',
  },
  imageIcon: {
    height: 85,
    width: 85,
    marginRight: 20,
  },
  viewPrediction: {
    marginHorizontal: 20,
    marginVertical: 5,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#ffffff',
  },
  textDetail: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 13,
    color: '#ffffff80',
    lineHeight: 16,
  },
});
