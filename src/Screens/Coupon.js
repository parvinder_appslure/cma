import React, {useState} from 'react';
import {
  FlatList,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const Coupon = ({navigation}) => {
  const [person, setPerson] = useState([
    {
      key: 1,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
    {
      key: 2,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
    {
      key: 3,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
    {
      key: 4,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
    {
      key: 5,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
    {
      key: 6,
      recharge: 'For recharges over 2000',
      amount: '2000 get 2500',
      validity: 'Valid to: 20/12/2020',
      use: 'Use it',
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header onPress={navigation.goBack} title={'Coupon'} />

        <FlatList
          data={person}
          numColumns={1}
          renderItem={({item}) => {
            return (
              <>
                <ImageBackground
                  source={require('../assets/bg.png')}
                  style={styles.bg}>
                  <View style={styles.flatlistview}>
                    <View>
                      <Text style={styles.bgText}>{item.recharge}</Text>
                      <Text style={styles.bgText_1}>{item.amount}</Text>
                      <Text style={styles.bgText}>{item.validity}</Text>
                    </View>
                    <View style={styles.bgView}>
                      <Text style={styles.bgText_2}>{item.use}</Text>
                    </View>
                  </View>
                </ImageBackground>
              </>
            );
          }}
        />
      </ScrollView>
    </MainView>
  );
};

export default Coupon;

const styles = StyleSheet.create({
  bg: {
    width: '95%',
    height: 106,
    resizeMode: 'contain',
    marginLeft: 15,
    marginTop: 10,
  },
  flatlistview: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 15,
  },
  bgText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.5,
  },
  bgText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 25,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 5,
    marginBottom: 5,
  },
  bgView: {
    width: '20%',
    height: 45,
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    marginLeft: 50,
    alignSelf: 'center',
  },
  bgText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000',
    marginTop: 13,
    marginLeft: 10,
  },
});
