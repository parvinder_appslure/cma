import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import DashedLine from 'react-native-dashed-line';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';

const CompleteOrder = ({navigation}) => {
  const [person, setPerson] = useState([
    {
      key: 1,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
    {
      key: 2,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
    {
      key: 3,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
    {
      key: 4,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
    {
      key: 5,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
    {
      key: 6,
      order: 'Order #123456',
      date: '06/03/2021 - 10:30 AM',
      status: 'Status: ',
      Completed: 'Completed',
      source: require('../assets/img.png'),
      stonetype: '6 Ratti Blue Sapphire (Neelam) Gen..',
      quantity: 'Quantity : 1',
      mrp: 'MPR:',
      amount: '1800/-',
      total: 'Total: ',
      price: '₹3600/- ',
    },
  ]);
  return (
    <MainView>
      <StatusBarLight />
      <FlatList
        data={person}
        numColumns={1}
        renderItem={({item}) => {
          return (
            <>
              <ImageBackground
                source={require('../assets/order-bg.png')}
                style={styles.bg}>
                <View style={styles.ftview}>
                  <Text style={styles.ftText}>{item.order}</Text>
                  <Text style={styles.ftText1}>{item.date}</Text>
                </View>

                <View style={{padding: '5%', marginHorizontal: 10}}>
                  <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                </View>

                <Text style={styles.ftText2}>
                  {item.status}
                  <Text style={[styles.ftText2, {color: '#00C327'}]}>
                    {item.Completed}
                  </Text>
                </Text>

                <View style={styles.ftview_1}>
                  <Image source={item.source} style={styles.ftImg} />
                  <View style={styles.ftview_2}>
                    <Text style={styles.ftText_2}>{item.stonetype}</Text>
                    <Text style={styles.ftText_1}>{item.quantity}</Text>
                    <Text style={styles.ftText_1}>
                      {item.mrp}
                      <Text style={[styles.ftText_1, {color: '#6266F9'}]}>
                        {item.amount}
                      </Text>
                    </Text>
                  </View>
                </View>

                <View style={{padding: '5%', marginHorizontal: 10}}>
                  <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                </View>

                <View style={styles.ftview_1}>
                  <Image source={item.source} style={styles.ftImg} />
                  <View style={styles.ftview_2}>
                    <Text style={styles.ftText_2}>{item.stonetype}</Text>
                    <Text style={styles.ftText_1}>{item.quantity}</Text>
                    <Text style={styles.ftText_1}>
                      {item.mrp}
                      <Text style={[styles.ftText_1, {color: '#6266F9'}]}>
                        {item.amount}
                      </Text>
                    </Text>
                  </View>
                </View>

                <View style={{padding: '5%', marginHorizontal: 10}}>
                  <DashedLine dashColor={'#6F6F7B'} dashLength={5} />
                </View>

                <Text style={styles.ftText_3}>
                  {item.total}
                  <Text style={[styles.total, {color: '#6266F9'}]}>
                    {item.price}
                  </Text>
                </Text>
              </ImageBackground>
            </>
          );
        }}
      />
    </MainView>
  );
};

export default CompleteOrder;

const styles = StyleSheet.create({
  bg: {
    width: 335,
    height: 370,
    resizeMode: 'contain',
    // marginLeft: 15,
    marginTop: 10,
    alignSelf: 'center',
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 30,
    // marginLeft: 10,
    // alignSelf: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.5,
  },
  dash: {
    borderColor: '#6F6F7B',
    marginHorizontal: 20,
    borderStyle: 'dotted',
    borderWidth: 1,
    marginTop: 10,
  },
  ftText2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginHorizontal: 30,
  },
  ftImg: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 30,
    // backgroundColor: 'yellow',
    marginTop: 10,
  },
  ftview_2: {
    marginHorizontal: 10,
    // backgroundColor: 'yellow',
    width: '70%',
  },
  ftText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    opacity: 0.8,
    marginTop: 3,
  },
  ftText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#FFFFFF',
    marginHorizontal: 30,
  },
});
