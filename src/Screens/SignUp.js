import React, {useState} from 'react';

import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  StatusBar,
  Image,
  TouchableOpacity,
} from 'react-native';
import {validateEmail} from '../component/CommonFunction';
import Loader from '../component/Loader';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {ButtonStyle, CustomInputField, MainView} from '../Custom/CustomView';
import {Api} from '../services/Api';

const SignUp = ({navigation}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    mobile: '',
    dob: '',
    gender: '',
    birth_time: '',
    place_of_birth: '',
    referral_code: '',
    showPicker: false,
    mode: '',
    date: '',
    isLoading: false,
  });

  const toggleLoader = isLoading => setState({...state, isLoading});
  // const showDateTimePicker = () => (
  //   <DateTimePicker
  //     value={state.date || new Date()}
  //     maximumDate={new Date()}
  //     mode={state.mode}
  //     display="spinner"
  //     is24Hour={false}
  //     onChange={({type, nativeEvent}) => {
  //       if (type === 'set') {
  //         const key = state.mode === 'time' ? 'birth_time' : 'dob';
  //         const value =
  //           state.mode === 'time'
  //             ? moment(nativeEvent.timestamp).format('hh:mm a')
  //             : moment(nativeEvent.timestamp).format('YYYY-MM-DD');
  //         setState({
  //           ...state,
  //           showPicker: false,
  //           [key]: value,
  //           date: nativeEvent.timestamp,
  //         });
  //       } else {
  //         setState({...state, showPicker: false});
  //       }
  //     }}
  //   />
  // );
  const onSaveHandler = async () => {
    const {
      name,
      email,
      dob = '',
      mobile,
      gender,
      birth_time = '',
      place_of_birth = '',
      referral_code = '',
    } = state;
    if (name === '') {
      alert('Please enter your name');
      return;
    }
    if (!validateEmail(email)) {
      alert('Please enter valid email');
      return;
    }

    if (mobile.length !== 10 && !isNaN(mobile)) {
      alert('Please enter valid Mobile number');
      return;
    }
    // if (dob === '') {
    //   alert('Please select date of birth');
    //   return;
    // }
    // if (birth_time === '') {
    //   alert('Please select time of birth');
    //   return;
    // }
    // if (place_of_birth === '') {
    //   alert('Please enter your place of birth');
    //   return;
    // }
    if (gender === '') {
      alert('Please select your gender');
      return;
    }
    const body = {
      name,
      email,
      mobile,
      dob: '',
      gender,
      birth_time: '',
      place_of_birth: '',
      referral_code: '',
    };
    consolejson(body);
    toggleLoader(true);
    const response = await Api.register(body);
    toggleLoader(false);
    consolejson(response);
    return;
    const {status = false, message = 'Something went wrong'} = response;
    if (status) {
      navigation.reset({
        index: 0,
        routes: [{name: 'Splash'}],
      });
    } else {
      alert(message);
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.isLoading} />
      <ScrollView>
        {/* <Text style={styles.toptext}>SKIP</Text> */}

        <Text style={styles.bgtext}>SIGNUP</Text>
        <Text style={styles.subtext}>Complete Your Profile!</Text>
        <CustomInputField
          label="Name"
          defaultValue={state.name}
          onChangeText={name => setState({...state, name})}
        />

        <CustomInputField
          label="Email"
          defaultValue={state.email}
          onChangeText={email => setState({...state, email})}
        />
        <CustomInputField
          label="Mobile Number"
          keyboardType={'phone-pad'}
          maxLength={10}
          defaultValue={state.mobile}
          onChangeText={mobile => setState({...state, mobile})}
        />

        <Text style={styles.middletext}>Gender</Text>

        <View style={styles.viewGender}>
          <TouchableOpacity
            style={{marginRight: 30}}
            onPress={() => setState({...state, gender: 'male'})}>
            <Image
              source={require('../assets/male.png')}
              style={styles.imageGender}
            />
            {state.gender === 'male' && (
              <Image
                source={require('../assets/checked.png')}
                style={styles.imageTick}
              />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setState({...state, gender: 'female'})}>
            <Image
              source={require('../assets/female.png')}
              style={styles.imageGender}
            />
            {state.gender === 'female' && (
              <Image
                source={require('../assets/checked.png')}
                style={styles.imageTick}
              />
            )}
          </TouchableOpacity>
        </View>

        {ButtonStyle('SIGN UP', '#3F55F6', '#FFFFFF', onSaveHandler)}
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.bottomtext}>
            Already have an account?
            <Text style={[styles.bottomtext, {color: '#6266F9'}]}>
              {' '}
              Login Now
            </Text>
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  toptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '600',
    color: '#FFFFFF',
    paddingTop: StatusBar.currentHeight + 20,
    alignSelf: 'flex-end',
    marginHorizontal: 25,
  },
  bgtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 72,
    fontWeight: '600',
    color: '#6F6F7B',
    opacity: 0.1,
    marginTop: 25,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 24,
    fontWeight: '600',
    color: '#FFFFFF',
    marginTop: -40,
    marginHorizontal: 30,
  },
  containerStyle: {
    width: '90%',
    height: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 60,
    paddingBottom: 20,
  },
  inputContainerStyle: {
    marginHorizontal: 30,
  },
  middletext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '400',
    color: '#FFFFFF',
    marginTop: 25,
    marginHorizontal: 25,
  },
  bottomtext: {
    fontSize: 14,
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    color: '#FFFFFF',
    marginBottom: 30,
    marginTop: 20,
    alignSelf: 'center',
  },

  viewGender: {
    flexDirection: 'row',
    margin: 30,
    marginBottom: 10,
  },
  imageGender: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
  },
  imageTick: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    position: 'absolute',
    right: 2,
    top: 2,
  },
});

export default SignUp;
