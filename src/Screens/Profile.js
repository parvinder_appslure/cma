import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableOpacityBase,
} from 'react-native';
import {useSelector} from 'react-redux';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {MainView} from '../Custom/CustomView';

const Profile = ({navigation}) => {
  const {userDetail} = useSelector(store => store);
  consolejson(userDetail);
  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
          <Image
            source={require('../assets/edit.png')}
            style={styles.topimage}
          />
        </TouchableOpacity>
        <Image source={{uri: userDetail?.imageUrl}} style={styles.userimage} />

        <Text style={styles.username}>{userDetail.name}</Text>
        <Text style={[styles.username, {fontSize: 14}]}>
          {userDetail?.phone}
        </Text>

        <View style={styles.subView}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Wallet')}
            style={styles.coin}>
            <Image
              source={require('../assets/coin.png')}
              style={styles.coinimage}
            />
            <View style={styles.subviewGap}>
              <Text style={styles.balance}>Balance</Text>
              <Text
                style={[
                  styles.balance,
                  {fontSize: 18},
                ]}>{`\u20b9${userDetail?.wallet}`}</Text>
            </View>
          </TouchableOpacity>

          <View style={styles.border} />

          <View style={styles.coin}>
            <Image
              source={require('../assets/coupon.png')}
              style={styles.coinimage}
            />
            <TouchableOpacity
              onPress={() => navigation.navigate('Coupon')}
              style={styles.subviewGap}>
              <Text style={styles.balance}>Coupon</Text>
              <Text style={[styles.balance, {fontSize: 18}]}>4</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.box}>
          <View style={styles.subBox}>
            <TouchableOpacity
              style={styles.superbox}
              onPress={() => navigation.navigate('EditProfile')}>
              <Image
                source={require('../assets/avatar.png')}
                style={styles.boximage}
              />
              <Text style={styles.boxText}>My Profile</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('TopTabNavigation')}
              style={styles.superbox}>
              <Image
                source={require('../assets/shop.png')}
                style={styles.boximage}
              />
              <Text style={styles.boxText}> Order History</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('BookingHistory')}
              style={styles.superbox}>
              <Image
                source={require('../assets/history.png')}
                style={styles.boximage}
              />
              <Text style={styles.boxText}> Booking History</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.box}>
          <View style={styles.subBox}>
            <View style={styles.superbox}>
              <TouchableOpacity onPress={() => navigation.navigate('UserList')}>
                <Image
                  source={require('../assets/group_menu.png')}
                  style={styles.boximage}
                />
              </TouchableOpacity>
              <Text style={styles.boxText}>User List</Text>
            </View>

            <TouchableOpacity
              style={styles.superbox}
              onPress={() => navigation.navigate('Support')}>
              <Image
                source={require('../assets/support.png')}
                style={styles.boximage}
              />
              <Text style={styles.boxText}>Support</Text>
            </TouchableOpacity>

            <TouchableOpacity
              // style={styles.superbox}
              onPress={() => navigation.navigate('Setting')}
              style={styles.superbox}>
              <Image
                source={require('../assets/settings.png')}
                style={styles.boximage}
              />
              <Text style={styles.boxText}>Settings</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.exchnageV}>
          <View style={styles.exchangebox}>
            <View>
              <Text style={[styles.balance, {fontSize: 16}]}>Settings</Text>
              <Text style={styles.exchangeText}>Get ₹200</Text>

              <View style={styles.exchnageview}>
                <Text style={[styles.balance, {fontSize: 16}]}>
                  Invite Friends
                </Text>
              </View>
            </View>
            <Image
              source={require('../assets/exchanging.png')}
              style={styles.exchangeimage}
            />
          </View>
        </View>
      </ScrollView>
    </MainView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  topimage: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    marginTop: 50,
    alignSelf: 'flex-end',
    marginHorizontal: 20,
  },
  userimage: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    marginTop: 20,
    alignSelf: 'center',
  },
  username: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
  coinimage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  balance: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: '400',
    color: '#ffffff',
  },
  coin: {
    flexDirection: 'row',
  },
  subView: {
    justifyContent: 'space-between',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  border: {
    height: '100%',
    borderColor: '#FFFFFF',
    opacity: 0.3,
    borderWidth: 1,
  },
  subviewGap: {
    marginLeft: 10,
  },
  box: {
    backgroundColor: '#1A0836',
    borderRadius: 15,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  boxText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 10,
    color: '#ffffff',
  },
  subBox: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  boximage: {
    width: 25,
    height: 21,
    resizeMode: 'contain',
  },
  superbox: {
    alignItems: 'center',
  },
  exchangeimage: {
    width: 90,
    height: 90,
    resizeMode: 'contain',
  },
  exchangebox: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  exchangeText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 30,
    fontWeight: '500',
    color: '#ffffff',
  },
  exchnageview: {
    padding: 10,
    backgroundColor: '#3F55F6',
    borderRadius: 20,
    alignItems: 'center',
    marginTop: 10,
  },
  exchnageV: {
    backgroundColor: '#1A0836',
    borderRadius: 15,
    padding: 20,
    marginTop: 40,
    marginHorizontal: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    marginBottom: 20,
    elevation: 6,
  },
});
