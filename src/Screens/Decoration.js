import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {useState} from 'react';
import {Header, MainView} from '../Custom/CustomView';

const Decoration = ({navigation}) => {
  const [bestSllersList, setBestSllersList] = useState([
    {
      keys: 12,
    },
    {
      keys: 13,
    },
    {
      keys: 14,
    },
    {
      keys: 15,
    },
  ]);

  const renderBestSellers = ({item}) => {
    //  console.log(JSON.stringify(item))
    return (
      <View
        style={{width: '48%', marginHorizontal: 3, marginVertical: 10}}
        //onPress={()=>navigation.navigate('')}
      >
        <View style={{width: '90%', alignSelf: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('AddToCart')}
            style={{width: '100%', borderRadius: 10}}>
            <Image
              style={styles.catseller}
              source={require('../assets/Rectangle.png')}
            />
          </TouchableOpacity>
          <Text style={styles.sellTextname}>Gutika</Text>

          <View
            style={{flexDirection: 'row', width: '100%', alignItems: 'center'}}>
            <Text style={styles.rupeeText}>₹</Text>
            <Text style={styles.moneyText}>900</Text>
            <Text style={styles.rupeeTextcut}>₹</Text>
            <Text style={styles.moneyTextcutted}>900</Text>

            <View style={{alignItems: 'flex-end', width: 65}}>
              <Text style={styles.rupeepercentage}>30%OFF</Text>
            </View>
          </View>
          <TouchableOpacity
            style={{
              width: 100,
              justifyContent: 'center',
              backgroundColor: '#E91E63',
              borderRadius: 5,
              marginTop: 6,
            }}>
            <Text
              style={{
                color: '#FFF',
                fontSize: 12,
                lineHeight: 16,
                alignSelf: 'center',
                marginVertical: 5,
                fontFamily: 'Avenir-Heavy',
                fontWeight: '700',
              }}>
              just launched
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <MainView>
      <View
        style={{
          height: 54,
          width: Dimensions.get('window').width,
          marginTop: 20,
          flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <View
          style={{
            width: '83%',
            flexDirection: 'row',
            marginLeft: 20,
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={{height: 22, width: 12, resizeMode: 'contain'}}
              source={require('../assets/back.png')}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: '#FFFFFF',
              fontSize: 20,
              lineHeight: 30,
              marginLeft: 20,
              fontFamily: 'Avenir-Heavy',
              fontWeight: '700',
            }}>
            Decoration
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('ReviewYourOrder')}>
          <View style={{alignSelf: 'flex-end'}}>
            <Image
              style={{height: 24, width: 25, resizeMode: 'contain'}}
              source={require('../assets/cart.png')}
            />
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView>
        <View style={{width: '100%', alignSelf: 'center'}}>
          <FlatList
            style={{width: '100%'}}
            data={bestSllersList}
            numColumns={2}
            horizontal={false}
            showsVerticalScrollIndicator={false}
            renderItem={renderBestSellers}
          />
        </View>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  catseller: {
    borderRadius: 10,
    marginVertical: 2,
    width: '100%',
    height: 218,

    resizeMode: 'contain',
  },
  catImgs: {
    width: 98,
    height: 98,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  catImg: {
    width: 68,
    height: 68,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  catTexts: {
    marginBottom: 10,
    alignSelf: 'center',
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Avenir-Heavy',
    color: '#FFF',
    marginTop: 10,
    fontWeight: '900',
  },
  catText: {
    alignSelf: 'center',
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Avenir-Heavy',
    color: '#FFF',
    marginTop: 10,
    fontWeight: '900',
  },
  catTextss: {
    alignSelf: 'center',
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  sellTextname: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  rupeeText: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeeTextcut: {
    marginLeft: 5,
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeepercentage: {
    marginTop: 8,
    marginLeft: 5,
    fontSize: 11,
    lineHeight: 15,
    fontFamily: 'Avenir-Medium',
    color: '#3F55F6',
    fontWeight: '500',
  },

  moneyText: {
    fontSize: 20,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  moneyTextcutted: {
    fontSize: 20,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '500',
    textDecorationLine: 'line-through',
  },
});

export default Decoration;
