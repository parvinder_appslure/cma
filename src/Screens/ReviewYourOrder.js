import React, {useState, useEffect} from 'react';
import {useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Dimensions,
  FlatList,
  TouchableOpacity,
  interpolateColor,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import RBSheet from 'react-native-raw-bottom-sheet';

const ReviewYourOrder = ({navigation}) => {
  const refRBSheet = useRef();

  const [bestSllersList, setBestSllersList] = useState([
    {
      keys: 12,

      name: 'Nazar Dosh Hanging Yantra',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
    {
      keys: 13,
      name: 'Nazar Dosh Hanging Yantra',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
    {
      keys: 14,
      name: 'Nazar Dosh Hanging Yantra',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
    {
      keys: 15,
      name: 'Nazar Dosh Hanging Yantra',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
    {
      keys: 16,
      name: 'Nazar Dosh Hanging Yantra',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
  ]);

  const [person, setPerson] = useState([
    {
      key: 1,
      name: 'Fengshui',
      seeAll: 'Size: ALL',
      price: '299',
      cuttedPrice: '599',
      source: require('../assets/img.png'),
    },
  ]);

  const renderBestSellers = ({item}) => {
    //  console.log(JSON.stringify(item))
    return (
      <TouchableOpacity
        //onPress={()=>navigation.navigate('')}

        style={{marginHorizontal: 12}}>
        <View style={{width: 104}}>
          <View style={{width: 104, justifyContent: 'center'}}>
            <ImageBackground
              imageStyle={{borderRadius: 3}}
              style={styles.catseller}
              source={item.source}>
              <TouchableOpacity
                style={{
                  height: 20,
                  width: 20,
                  justifyContent: 'center',
                  backgroundColor: '#3F55F6',
                  borderRadius: 10,
                  alignSelf: 'flex-end',
                  marginRight: 3,
                  marginTop: 4,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    lineHeight: 20,
                    fontFamily: 'Avenir-Heavy',
                    textAlign: 'center',
                    color: '#FFF',
                    fontWeight: '900',
                  }}>
                  +
                </Text>
              </TouchableOpacity>
            </ImageBackground>
          </View>
          <Text style={styles.sellTextname}>{item.name}</Text>

          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              alignItems: 'center',
              marginBottom: 10,
            }}>
            <Text style={styles.rupeeText}>₹</Text>
            <Text style={styles.moneyText}>{item.price}</Text>
            <Text style={styles.rupeeTextcut}>₹</Text>
            <Text style={styles.moneyTextcutted}>{item.cuttedPrice}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <Header onPress={navigation.goBack} title={'Review Your Order'} />
        <View
          style={{
            height: 0.6,
            width: '100%',
            alignSelf: 'center',
            backgroundColor: '#FFFFFF40',
          }}></View>

        <View style={{width: '100%'}}>
          <FlatList
            data={person}
            numColumns={1}
            renderItem={({item}) => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    width: '90%',
                    alignSelf: 'center',
                    alignItems: 'center',
                    marginTop: 15,
                  }}>
                  <Image
                    style={{
                      height: 70,
                      width: 70,
                      resizeMode: 'contain',
                      borderRadius: 10,
                    }}
                    source={item.source}
                  />

                  <View style={{flexDirection: 'column', width: '80%'}}>
                    <View
                      style={{
                        width: '90%',
                        alignSelf: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: 18,
                          lineHeight: 22,
                          fontFamily: 'Avenir-Medium',
                          color: '#FFF',
                          fontWeight: '500',
                        }}>
                        {item.name}
                      </Text>

                      <Text
                        style={{
                          fontSize: 20,
                          lineHeight: 24,
                          fontFamily: 'Avenir-Heavy',
                          color: '#FFF',
                          fontWeight: '900',
                        }}>
                        ₹ {item.price}
                      </Text>
                    </View>

                    <View
                      style={{
                        width: '90%',
                        alignSelf: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: 12,
                          lineHeight: 16,
                          fontFamily: 'Avenir-Medium',
                          color: '#FFF',
                          fontWeight: '500',
                        }}>
                        {item.seeAll}
                      </Text>

                      <Text
                        style={{
                          fontSize: 18,
                          lineHeight: 22,
                          fontFamily: 'Avenir-Normal',
                          color: '#FFFFFF50',
                          fontWeight: '900',
                          textDecorationLine: 'line-through',
                        }}>
                        ₹{item.cuttedPrice}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
          />
        </View>

        <View style={{backgroundColor: '#13042A', marginTop: 18}}>
          <Text style={styles.middleText}>You may also like</Text>

          <FlatList
            style={{width: '100%'}}
            data={bestSllersList}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={renderBestSellers}
          />
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Heavy',
              color: '#FFF',
              fontWeight: '900',
            }}>
            Item Total
          </Text>
          <Text
            style={{
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Heavy',
              color: '#FFF',
              fontWeight: '900',
            }}>
            ₹799
          </Text>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 15,
              lineHeight: 19,
              fontFamily: 'Avenir-Bold',
              color: '#FFF',
              fontWeight: '700',
            }}>
            Total Discount (-)
          </Text>
          <Text
            style={{
              fontSize: 15,
              lineHeight: 19,
              fontFamily: 'Avenir-Bold',
              color: '#FFF',
              fontWeight: '700',
            }}>
            ₹100.00
          </Text>
        </View>
        <View
          style={{
            height: 0.6,
            width: '90%',
            marginVertical: 10,
            alignSelf: 'center',
            backgroundColor: '#FFFFFF40',
          }}></View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{width: '50%', flexDirection: 'row'}}>
            <Image
              style={{height: 18, width: 18, resizeMode: 'contain'}}
              source={require('../assets/uncircle.png')}
            />
            <Text
              style={{
                fontSize: 16,
                lineHeight: 20,
                fontFamily: 'Avenir-Heavy',
                color: '#FFF',
                fontWeight: '900',
                marginLeft: 10,
              }}>
              Use Wallet Balance
            </Text>
          </View>

          <View style={{width: '50%'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                borderWidth: 1,
                borderColor: '#FFF',
                alignSelf: 'flex-end',
                borderRadius: 20,
                backgroundColor: '#0E0220',
              }}>
              <Image
                style={{
                  height: 25,
                  width: 25,
                  marginHorizontal: 10,
                  marginVertical: 4,
                  alignSelf: 'center',
                  resizeMode: 'contain',
                }}
                source={require('../assets/Empty-Gold-Coin-PNG-Image.png')}
              />
              <Text
                style={{
                  fontSize: 20,
                  lineHeight: 29,
                  fontFamily: 'Avenir-Heavy',
                  color: '#FFF',
                  fontWeight: '900',
                  marginVertical: 4,
                  marginRight: 5,
                }}>
                O
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            height: 0.6,
            width: '90%',
            marginTop: 15,
            alignSelf: 'center',
            backgroundColor: '#FFFFFF40',
          }}></View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 10,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 15,
              lineHeight: 19,
              fontFamily: 'Avenir-Bold',
              color: '#FFF',
              fontWeight: '700',
            }}>
            Total Payable
          </Text>
          <Text
            style={{
              fontSize: 15,
              lineHeight: 19,
              fontFamily: 'Avenir-Bold',
              color: '#FFF',
              fontWeight: '700',
            }}>
            ₹699.00
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => refRBSheet.current.open()}
          //onPress={()=>navigation.navigate('Payment')}
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: 50,
            marginBottom: 40,
            justifyContent: 'center',
            backgroundColor: '#3F55F6',
            borderRadius: 10,
          }}>
          <Text
            style={{
              alignSelf: 'center',
              marginVertical: 15,
              fontSize: 18,
              lineHeight: 22,
              fontFamily: 'Avenir-Medium',
              color: '#FFF',
              fontWeight: '500',
            }}>
            Select Address
          </Text>
        </TouchableOpacity>

        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
            draggableIcon: {
              //     backgroundColor: "#FFF"
            },
          }}>
          <View
            style={{
              height: 10,
              width: 10,
              marginRight: 5,
              marginTop: 40,
              alignSelf: 'flex-end',
            }}>
            <TouchableOpacity>
              <Image
                style={{height: 10, width: 6, resizeMode: 'contain'}}
                source={require('../assets/dropimg.png')}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: -20,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Image
              style={{height: 21, width: 15, resizeMode: 'contain'}}
              source={require('../assets/placeholder.png')}
            />

            <Text
              style={{
                marginLeft: 10,
                fontSize: 18,
                lineHeight: 22,
                fontFamily: 'Avenir-Heavy',
                color: '#FFFFFF',
                fontWeight: '900',
              }}>
              HOME
            </Text>
            <View
              style={{
                height: 5,
                width: 5,
                borderRadius: 2.5,
                backgroundColor: '#FFF',
                marginLeft: 12,
                marginRight: 8,
              }}></View>
            <View style={{width: 210}}>
              <Text
                style={{
                  fontSize: 16,
                  lineHeight: 20,
                  fontFamily: 'Avenir-Medium',
                  color: '#FFFFFF',
                  fontWeight: '500',
                }}>
                B5/123, Sec-9, Rohini New…
              </Text>
            </View>
          </View>

          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 10,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Image
              style={{height: 17, width: 17, resizeMode: 'contain'}}
              source={require('../assets/history.png')}
            />

            <Text
              style={{
                marginLeft: 10,
                fontSize: 18,
                lineHeight: 22,
                fontFamily: 'Avenir-Heavy',
                color: '#FFFFFF',
                fontWeight: '900',
              }}>
              DELIVERY
            </Text>
            <View
              style={{
                height: 5,
                width: 5,
                borderRadius: 2.5,
                backgroundColor: '#FFF',
                marginLeft: 12,
                marginRight: 8,
              }}></View>

            <Text
              style={{
                fontSize: 16,
                lineHeight: 20,
                fontFamily: 'Avenir-Medium',
                color: '#FFFFFF',
                fontWeight: '500',
              }}>
              21 May, 09:00 PM
            </Text>
          </View>

          <TouchableOpacity
            //onPress={() => refRBSheet.current.close()}
            onPress={() => navigation.navigate('Payment')}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 50,
              marginBottom: 30,
              justifyContent: 'center',
              backgroundColor: '#3F55F6',
              borderRadius: 10,
            }}>
            <Text
              style={{
                alignSelf: 'center',
                marginVertical: 15,
                fontSize: 18,
                lineHeight: 22,
                fontFamily: 'Avenir-Medium',
                color: '#FFF',
                fontWeight: '500',
              }}>
              Pay ₹699.00
            </Text>
          </TouchableOpacity>
        </RBSheet>
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  sellTextname: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
    // opacity:
  },
  rupeeText: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeeTextcut: {
    marginLeft: 5,
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Normal',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '400',
  },

  rupeepercentage: {
    marginTop: 8,
    marginLeft: 5,
    fontSize: 11,
    lineHeight: 15,
    fontFamily: 'Avenir-Medium',
    color: '#3F55F6',
    fontWeight: '500',
  },

  moneyText: {
    fontSize: 20,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFF',
    marginTop: 7,
    fontWeight: '500',
  },
  moneyTextcutted: {
    fontSize: 14,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    color: '#FFFFFF50',
    marginTop: 7,
    fontWeight: '500',
    textDecorationLine: 'line-through',
  },
  catseller: {
    borderRadius: 3,
    marginVertical: 2,
    width: 104,
    height: 104,

    resizeMode: 'contain',
  },
  middleText: {
    marginBottom: 18,
    marginLeft: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    lineHeight: 22,
    fontWeight: '900',
    color: '#FFFFFF',
    marginTop: 17,
  },
  topView: {
    backgroundColor: '#3F55F6',
    paddingBottom: 15,
    padding: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
  topimage: {
    width: 31,
    height: 31,
    resizeMode: 'contain',
  },
  topText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
    marginLeft: 10,
  },
  topView_1: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  marginView: {
    marginHorizontal: 20,
  },
  topText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
    textDecorationLine: 'underline',
    marginTop: 20,
  },
  middleView: {
    width: '90%',
    alignSelf: 'center',
  },

  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FFFFFF',
  },

  bottomText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
    opacity: 0.8,
  },

  walletImage: {
    width: 104,
    height: 104,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  historyview: {
    flexDirection: 'row',
    marginLeft: -10,
    alignItems: 'center',
    marginTop: 20,
  },
  amount: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#7ED321',
    marginHorizontal: 40,
  },
});

export default ReviewYourOrder;
