import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  ScrollView,
  Image,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {BottomButton, Header, MainView} from '../Custom/CustomView';

const RechargeWallet = ({navigation}) => {
  const [state, setState] = useState({
    amount: '',
  });
  useEffect(() => {
    console.log('amount', state);
  });
  const [person, setPerson] = useState([
    {
      title: '₹ 200',
      key: 1,
    },
    {
      title: '₹ 200',
      key: 2,
    },
    {
      title: '₹ 200',
      key: 3,
    },
    {
      title: '₹ 200',
      key: 4,
    },
    {
      title: '₹ 200',
      key: 5,
    },
    {
      title: '₹ 200',
      key: 6,
    },
  ]);

  return (
    <MainView>
      <StatusBarLight />
      <Header onPress={navigation.goBack} title={'Recharge Wallet'} />

      <ScrollView>
        <View style={styles.topv1_Style}>
          <View style={styles.topview}>
            <Image
              source={require('../assets/wallet.png')}
              style={styles.imag}
            />
            <Text style={styles.toptext}>₹ 100</Text>
            <Text style={styles.top1}>Wallet Balance</Text>
          </View>

          <Text style={styles.text4_Style}>Enter Amount</Text>
          <TextInput
            keyboardType="number-pad"
            value={state.amount}
            onChangeText={text => setState({...setState, amount: text})}
            //placeholder="hello"
            underlineColorAndroid="#CED4E2"
            fontSize={20}
            margin={-3}
          />

          <Text style={styles.text5_Style}>
            Choose from the available wallet balance.
          </Text>

          <FlatList
            data={person}
            numColumns={3}
            renderItem={({item}) => {
              return (
                <View
                  style={{
                    width: '30%',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    marginTop: 10,
                    borderColor: '#CED4E2',
                    padding: 7,
                    borderRadius: 5,
                  }}>
                  <Text
                    style={{
                      fontSize: 22,
                      textAlign: 'center',
                      color: '#6F6F7B',
                      fontWeight: '500',
                    }}>
                    {item.title}
                  </Text>
                </View>
              );
            }}
          />
        </View>
        <BottomButton
          onPress={() => navigation.navigate('Payment')}
          bottomtitle={'Pay Now'}
        />
      </ScrollView>
    </MainView>
  );
};

export default RechargeWallet;

const styles = StyleSheet.create({
  topv1_Style: {
    width: '90%',
    alignSelf: 'center',
  },
  topview: {
    // width: '100%',
    backgroundColor: '#13042A',
    padding: 10,
    borderRadius: 10,
  },
  topi_Style: {
    width: 12,
    height: 20.5,
    resizeMode: 'contain',
    //marginLeft: 15,
  },
  text_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: 'bold',
    marginLeft: 15,
  },
  topv3_Style: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text1_Style: {
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 30,
    opacity: 0.9,
    marginBottom: 7,
  },
  text2_Style: {
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
    opacity: 0.9,
  },
  text3_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '500',
    color: '#7ED321',
    marginTop: 10,
  },
  view_Style: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image_Style: {
    width: 66,
    height: 74,
    resizeMode: 'contain',
    //marginLeft: 15,
  },
  text4_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '400',
    color: '#6F6F7B',
    marginTop: 70,
  },
  text5_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '400',
    color: '#6F6F7B',
    marginTop: 30,
  },
  bv_Style: {
    width: '100%',
    backgroundColor: '#FFD00D',
    alignSelf: 'center',
    marginTop: 50,
    padding: 8,
    borderRadius: 5,
  },
  text6_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center',
  },
  imag: {
    width: 70,
    height: 65,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  toptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 45,
    fontWeight: '700',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 10,
  },
  top1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.9,
    // marginBottom: 7,
    color: '#FFFFFF',
    alignSelf: 'center',
  },
});
