import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import Explore from '../Screens/Explore';
import Sage from '../Screens/Sage';
import Shop from '../Screens/Shop';
import Profile from '../Screens/Profile';

const iconPath = {
  ah: require('../assets/direction.png'),
  ih: require('../assets/direction1.png'),
  as: require('../assets/sage1.png'),
  is: require('../assets/sage2.png'),
  ae: require('../assets/shop1.png'),
  ie: require('../assets/shop.png'),
  ap: require('../assets/avatar1.png'),
  ip: require('../assets/avatar.png'),
};

const TabIcon = source => (
  <Image
    source={source}
    style={{
      height: 18,
      width: 18,
      resizeMode: 'contain',
    }}
  />
);

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Explore"
      screenOptions={{
        keyboardHidesTabBar: true,
        tabBarStyle: {
          backgroundColor: '#0E0220',
        },
        tabBarItemStyle: {
          paddingVertical: 4,
        },
        tabBarActiveTintColor: '#3F55F6',
        tabBarInactiveTintColor: '#ffffff',
        tabBarLabelStyle: {
          fontSize: 11,
          fontWeight: '900',
          fontFamily: 'Avenir-Heavy',
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Explore}
        options={{
          headerShown: false,
          tabBarLabel: 'Explore',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ah : iconPath.ih),
        }}
      />

      <Tab.Screen
        name="Sage"
        component={Sage}
        options={{
          tabBarLabel: 'Sage',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.as : iconPath.is),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Shop"
        component={Shop}
        options={{
          headerShown: false,
          tabBarLabel: 'E-Shop',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ae : iconPath.ie),
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
          tabBarLabel: 'Profile',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ap : iconPath.ip),
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});
