import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MyOrderOngoing from '../Screens/MyOrderOngoing';
import CompleteOrder from '../Screens/CompleteOrder';
import {Header} from '../Custom/CustomView';

const Tab = createMaterialTopTabNavigator();

const TopTabNavigation = ({navigation}) => {
  return (
    <>
      <Header onPress={navigation.goBack} title={'My Order'} />
      <Tab.Navigator
        // initialRouteName="Explore"

        tabBarOptions={{
          keyboardHidesTabBar: true,
          //showLabel: false,
          inactiveTintColor: '#ffffff',
          activeTintColor: '#6266F9',

          labelStyle: {
            fontFamily: 'Avenir-Medium',
            fontWeight: '500',
            fontSize: 14,

            // backgroundColor: 'yellow',
            // color: true ? 'red' : 'yellow',
          },
          style: {
            backgroundColor: '#13042A',
          },

          //   activeBackgroundColor: '#0E0220',
          //   inactiveBackgroundColor: '#0E0220',
        }}>
        <Tab.Screen
          name="MyOrderOngoing"
          component={MyOrderOngoing}
          options={{
            headerShown: false,
            tabBarLabel: 'ONGOING ORDER',
          }}
        />

        <Tab.Screen
          name="CompleteOrder"
          component={CompleteOrder}
          options={{
            tabBarLabel: 'COMPLETED ORDER',
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default TopTabNavigation;

const styles = StyleSheet.create({});
