const data = {
  userDetail: {},
  onlineAstrologer: [],
  banners: [],
  services: [],
  testimonials: [],
  specialities: [],
  netInfo: {
    details: {},
    isConnected: false,
    isInternetReachable: false,
    isWifiEnabled: false,
    type: '',
  },
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
  isLogin: true,
  socket: null,
  information: {
    privacy: '',
    terms_condition: '',
    about_us: '',
  },
  faq: [],
  language: [],
  categoryList: [],
  skillList: [],
  membersList: [],
  astroShop: [],
  myCart: {},
  addresses: [],
};
const reducer = (state = data, action) => {
  console.log('action::', action.type);
  switch (action.type) {
    case 'setNetInfo':
      return {
        ...state,
        netInfo: action.payload,
      };
    case 'setDeviceInfo':
      return {
        ...state,
        deviceInfo: action.payload,
      };
    case 'setHome':
      return {
        ...state,
        banners: action.payload.banners,
        services: action.payload.services,
        testimonials: action.payload.testimonials,
        language: action.payload.language,
        categoryList: action.payload.categoryList,
      };
    case 'setSpecialities':
      return {
        ...state,
        specialities: action.payload.specialities,
        skillList: action.payload.skillList,
      };
    case 'setUserDetail':
      return {
        ...state,
        userDetail: action.payload,
        isLogin: true,
      };
    case 'setLogout':
      return {
        ...state,
        userDetail: {},
        // isLogin: false,
      };
    case 'setWallet':
      return {
        ...state,
        userDetail: {
          ...state.userDetail,
          wallet: action.payload,
        },
      };
    case 'setInformation':
      return {
        ...state,
        information: action.payload,
      };
    case 'setFaq':
      return {
        ...state,
        faq: action.payload,
      };
    case 'setMembersList':
      return {
        ...state,
        membersList: action.payload,
      };
    case 'setAstroShop':
      return {
        ...state,
        astroShop: action.payload,
      };
    case 'cartUpdate':
      return {
        ...state,
        myCart: action.payload,
      };
    case 'addresses':
      return {
        ...state,
        addresses: action.payload,
      };
    case 'onlineAstrologer':
      return {
        ...state,
        onlineAstrologer: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
