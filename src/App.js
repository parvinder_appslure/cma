// import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {LogBox, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import store from './redux/store';
import * as actions from './redux/actions';
import NetInfo from '@react-native-community/netinfo';
import DeviceInfo from 'react-native-device-info';
import {senderID} from './services/Config';
import PushNotification from 'react-native-push-notification';
import StackNavigator from './navigator/StackNavigator';
import {io} from 'socket.io-client';
import {SocketContext} from './redux/context';
import {SOCKET_URL} from './services/Config';
window.consolejson = json => console.log(JSON.stringify(json, null, 2));

const App = () => {
  const socket = io(SOCKET_URL, {transports: ['websocket']});
  LogBox.ignoreLogs([
    'Warning:',
    'Cannot update a component from inside',
    'Animated: `useNativeDriver` was not specified',
  ]);
  LogBox.ignoreAllLogs(true);

  useEffect(() => {
    const netInfoSubscribe = NetInfo.addEventListener(
      state => store.dispatch(actions.SetNetInfo(state)),
      console.log('sdf'),
    );

    PushNotification.configure({
      onRegister: ({token}) => {
        console.log('firebase_token : ', token);
        store.dispatch(
          actions.SetDeviceInfo({
            id: DeviceInfo.getDeviceId(),
            token: token.toString(),
            model: DeviceInfo.getModel(),
            os: Platform.OS,
          }),
        );
      },
      onNotification: notification => {
        consolejson({notification});
        // PushNotification.localNotification({
        //   title,
        //   message,
        // });
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);

        // process the action
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      senderID,
      popInitialNotification: true,
      requestPermissions: true,
    });
    return () => {
      netInfoSubscribe();
    };
  }, []);

  return (
    <Provider store={store}>
      <SocketContext.Provider value={socket}>
        <StackNavigator />
      </SocketContext.Provider>
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
