import AsyncStorage from '@react-native-async-storage/async-storage';
import {request, requestMultipart} from './ApiSauce';
import {Dimensions} from 'react-native';
export const {width, height} = Dimensions.get('window');

export const AspectRatio = () => width / height;

// import {Subject} from 'rxjs';
// import {debounceTime} from 'rxjs';

export const Api = {
  home: () => request('/fetch_home'),
  specialities: json => request('/fetch_specialities', json),
  getSpecialities: json => request('/get_astrologer_by_speciality', json),
  astrologerDetails: json => request('/fetch_astrologer_details', json),
  otp: json => request('/send_auth_otp', json),
  resendOtp: json => request('/resend_otp', json),
  otpVerify: json => request('/verify_auth_otp', json),
  register: json => request('/edit_details_user', json),
  userProfile: json => request('/user_profile', json),
  rechargeWallet: json => request('/recharge_user_wallet', json),
  walletHistory: json => request('/wallet_history', json),
  notification: json => request('/fetch_notifications', json),
  astrologerSpeciality: json =>
    request('/fetch_astrologer_speciality_horoscope', json),
  astrologerAddMember: json => request('/add_member', json),
  horoscopePayableAmount: json =>
    request('/fetch_payable_amount_horoscope_new', json),
  horoscopeBookNew: json => request('/horoscope_book_new', json),
  information: json => request('/fetch_settings', json),
  faqs: json => request('/fetch_faqs', json),
  dailyPrediction: json =>
    request(
      'http://139.59.94.54/kundali_expert/astrology_api/daily_sun_sign_prediction',
      json,
    ),
  endLive: json => request('/end_live_session', json),
  addReview: json => request('/add_review', json),
  searchAstrologers: json => request('/search_astrologers', json),
  astrologerTimeSlot: json => request('/astrologer_time_slots', json),
  membersList: json => request('/fetch_members', json),
  astrologerPayableAmount: json => request('astrologer_payable_amount', json),
  bookPremiumAstrologer: json => request('book_premium_astrologer', json),
  getAstroShopCategories: json => request('get_astro_shop_categories', json),
  getProductsByCategory: json => request('get_products_by_category', json),
  fetchCart: json => request('fetch_product_cart', json),
  addToCart: json => request('add_to_cart', json),
  updateToCart: json => request('edit_qty_to_cart', json),
  deleteToCart: json => request('delete_cart', json),
  checkoutAstroshop: json => request('checkout_astroshop', json),
  astroshopOrders: json => request('fetch_astroshop_orders', json),
  fetchAddresses: json => request('fetch_addresses', json),
  addAddresses: json => request('add_address', json),
  editAddresses: json => request('edit_address', json),
  deleteAddresses: json => request('delete_address', json),
  setDefaultAddresses: json => request('set_default_address', json),
  updateProfile: formData => requestMultipart('edit_details_user', formData),
};
export const LocalStorage = {
  setToken: token => AsyncStorage.setItem('authToken', token),
  getToken: () => AsyncStorage.getItem('authToken'),
  setFirstTimeOpen: () => AsyncStorage.setItem('firstTimeOpen', 'false'),
  // getFirstTimeOpen: () => AsyncStorage.getItem('firstTimeOpen'),
  clear: AsyncStorage.clear,
};

// const onSearchEvent = new Subject().pipe(debounceTime(500));
