import React, {memo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  StatusBar,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {StatusBarDark} from '../component/CustomStatusBar';
// import {viewStyle} from '../styles/style';
const PujaBooking = ({navigation}) => {
  const OptionList = memo(({title, source, onPress, content, Price}) => (
    <TouchableOpacity onPress={onPress} style={styles.Optiontouch}>
      <Image source={source} style={styles.optionImage} />
      <Text style={styles.optionTitle}>{title}</Text>
      <Text style={styles.optioncontent}>{content}</Text>
      <Text style={styles.optioncontent}>{Price}</Text>
    </TouchableOpacity>
  ));

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <StatusBarDark />
        <View style={styles.headerView}>
          <View style={styles.alignmentView}>
            <TouchableOpacity
              style={{marginHorizontal: 20}}
              onPress={navigation.goBack}>
              <Image
                source={require('../Image/back.png')}
                style={styles.backimage}
              />
            </TouchableOpacity>

            <Text style={styles.toptext}>Puja Booking</Text>

            <TouchableOpacity
              style={{marginLeft: 'auto'}}
              onPress={navigation.goBack}>
              <Image
                source={require('../Image/search-2.png')}
                style={styles.topimage}
              />
            </TouchableOpacity>

            <TouchableOpacity style={{marginHorizontal: 20}}>
              <Image
                source={require('../Image/filter.png')}
                style={styles.topimage}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View>
          <OptionList source={require('../Image/banner.png')} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
  },
  headerView: {
    backgroundColor: '#4FA980',
    paddingTop: StatusBar.currentHeight + 10,
    paddingBottom: 20,
  },
  alignmentView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backimage: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  topimage: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  toptext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#FFFFFF',
  },
  middleText_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#1D1E2C',
  },
  middleView: {
    marginTop: 20,
    marginLeft: 10,
  },
  Optiontouch: {
    width: '90%',
    alignSelf: 'center',
    elevation: 5,
    backgroundColor: '#FFFFFF',
    // padding: 15,
    // marginBottom: 20,
    borderRadius: 10,
    // top: 10,
    marginTop: 20,
  },

  optionImage: {
    width: '100%',
    height: 120,
    resizeMode: 'contain',
    // alignSelf: 'center',
    right: 20,
  },
  optionTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  optioncontent: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#1E2432',
    opacity: 0.5,
  },
});

export default PujaBooking;
