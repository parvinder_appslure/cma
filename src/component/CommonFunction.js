const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
export const validateEmail = text => reg.test(text);
export const htmlParse = str => {
  str = str.replace(/\r\n|\n|\r/gm, '');
  str = str.replace(/\&nbsp;/gm, '');
  str = str.replace(/\<p>\<\/p\>/gm, '');
  return str;
};
